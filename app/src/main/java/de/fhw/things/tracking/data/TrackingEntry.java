package de.fhw.things.tracking.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Calendar;

import de.fhw.things.db.DBEntity;

/**
 * Klasse zur Datenhaltung eines Zeittracking-Eintrages.
 */
@SuppressWarnings("NullableProblems")
public class TrackingEntry extends DBEntity {

    /** Startzeitpunkt eines Zeittracking-Vorgangs. */
    @NonNull
    private Calendar begin;
    /** Endzeitpunkt eines Zeittracking-Vorgangs. */
    @Nullable
    private Calendar end;
    /** Projektname. */
    @Nullable
    private String project;
    /** Beschreibung. */
    @Nullable
    private String description;


    /**
     * Konstruktor, der sämtliche Felder einer Zeitnahme inititalisiert.
     *
     * @param id          ID des Objekts
     * @param begin       Startzeitpunkt des Tracking-Eintrags
     * @param end         Endzeitpunkt des Tracking-Eintrags
     * @param project     Projektname
     * @param description Beschreibung
     */
    public TrackingEntry(@Nullable Long id, @NonNull Calendar begin, @Nullable Calendar end, @Nullable String
            project, @Nullable String description) {
        setBegin(begin).setEnd(end).setProject(project).setDescription(description).setID(id);
    }

    /**
     * Konstruktor, der eine Zeitnahme ohne Projekt und Beschreibung erstellt.
     *
     * @param begin Startzeitpunkt des Tracking-Eintrags
     * @param end   Endzeitpunkt des Tracking-Eintrags
     */
    public TrackingEntry(@NonNull Calendar begin, @Nullable Calendar end) {
        this(null, begin, end, null, null);
    }

    /**
     * Erzeugt einen Zeiteintrag für ein gegebenes Projekt und eine Beschreibung.
     * @param project Projekt
     * @param description Beschreibung
     */
    public TrackingEntry(@NonNull String project, @NonNull String description){
        this(null, Calendar.getInstance(), null, project, description);
    }

    /**
     * Konstruktor für einen neuen Zeiteintrag, der sofort beginnt.
     */
    public TrackingEntry() {
        this(null, Calendar.getInstance(), null, null, null);
    }

    /**
     * @return Startzeitpunkt des Tracking-Eintrags
     */
    @NonNull
    public Calendar getBegin() {
        return this.begin;
    }

    /**
     * @param begin Startzeitpunkt des Tracking-Eintrags
     * @return Selbstreferenz
     */
    @NonNull
    public TrackingEntry setBegin(@NonNull Calendar begin) {
        if (this.end != null && begin.after(this.end))
            throw new IllegalArgumentException("Start darf nicht nach dem Ende liegen");
        this.begin = begin;
        return this;
    }

    /**
     * @return Endzeitpunkt des Tracking-Eintrags
     */
    @Nullable
    public Calendar getEnd() {
        return this.end;
    }

    /**
     * @param end Endzeitpunkt des Tracking-Eintrags
     * @return Selbstreferenz
     */
    @NonNull
    public TrackingEntry setEnd(@Nullable Calendar end) {
        if (this.begin.after(end)) throw new IllegalArgumentException("Ende muss nach Startzeitpunkt liegen");
        this.end = end;
        return this;
    }

    @Nullable
    public String getProject() {
        return this.project;
    }

    @NonNull
    public TrackingEntry setProject(@Nullable String project) {
        this.project = project;
        return this;
    }

    @Nullable
    public String getDescription() {
        return this.description;
    }

    @NonNull
    public TrackingEntry setDescription(@Nullable String description) {
        this.description = description;
        return this;
    }

    /**
     * @return Länge des Zeittracking-Eintrags in Millisekunden.
     */
    public final long getDuration() {
        Calendar end = (this.end != null) ? this.end : Calendar.getInstance();
        return end.getTimeInMillis() - this.begin.getTimeInMillis();
    }

    /**
     * @return Zeittracking läuft noch?
     */
    public final boolean isRunning() {
        return this.end == null;
    }

    /**
     * Setzt den Endzeitpunkt des Trackings.
     */
    public final void stopTracking(){
        setEnd(Calendar.getInstance());
    }
}
