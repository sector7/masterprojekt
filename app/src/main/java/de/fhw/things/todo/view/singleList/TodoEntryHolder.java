package de.fhw.things.todo.view.singleList;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;

/**
 * Klasse zur Verwaltung aller Felder eines TodoEntries.
 */
public class TodoEntryHolder {
    /** ID */
    public Long id;
    /** Löschen Button */
    public ImageButton delete;
    /** Checkbox zum Abhaken von Einträgen */
    public CheckBox done;
    /** editierbare Beschreibung */
    public EditText editText;
    /** Priorisierung */
    public Button prioritized;
}
