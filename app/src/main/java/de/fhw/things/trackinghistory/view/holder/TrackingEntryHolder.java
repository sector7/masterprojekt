package de.fhw.things.trackinghistory.view.holder;

import android.app.Activity;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;

import de.fhw.things.R;
import de.fhw.things.tracking.data.TrackingEntry;

import static de.fhw.things.commons.TimeFormatter.formatTime;

/**
 * Holder zum Zugriff auf die Bestandteile der Komponente zur Anzeige eines Zeiteintrags.
 */
public class TrackingEntryHolder {
    /** Übergeordnete Activity */
    private final Activity parent;
    /** View des gesamten Eintrages */
    public final LinearLayout entry;
    /** Checkbox des Eintrages */
    public final CheckBox deleteMark;
    /** View mit Start- und Endzeitpunkt*/
    public final LinearLayout interval;
    /** Projekt */
    public final AutoCompleteTextView project;
    /** Aufgabenbeschreibung */
    public final AutoCompleteTextView description;
    /** Arbeitsdauer */
    public final TextView duration;
    /** Startdatum */
    public final TextView beginDate;
    /** Startzeit */
    public final TextView beginTime;
    /** Enddatum */
    public final TextView endDate;
    /** Endzeit */
    public final TextView endTime;
    /** Button zum starten eines neuen Tracking-Eintrags mit gleicher Beschreibung */
    public final ImageButton restartTracking;
    /** Zugehöriger Tracking-Eintrag */
    public final TrackingEntry trackingEntry;

    /**
     * Erstellt den Holder für einen Eintrag.
     * @param parent Aufrufende Activity
     * @param trackingEntry Zeiteintrag
     * @param tag Frontend-Id
     */
    public TrackingEntryHolder(Activity parent, TrackingEntry trackingEntry, int tag) {
        this.parent = parent;
        this.trackingEntry = trackingEntry;

        // Layout auf den Tracking-Eintrag anwenden
        this.entry = (LinearLayout) View.inflate(parent, R.layout.layout_tracking_entry, null);
        // Checkbox
        this.deleteMark = (CheckBox) this.entry.findViewById(R.id.cb_markForDeletion);
        // Restart Button
        this.restartTracking = (ImageButton) this.entry.findViewById(R.id.restartTrackingButton);

        // Beide Zeitstempel
        this.interval = (LinearLayout) this.entry.findViewById(R.id.interval);
        // Anfangs- und Endzeitstempel
        LinearLayout begin = (LinearLayout) this.entry.findViewById(R.id.begin);
        LinearLayout end = (LinearLayout) this.entry.findViewById(R.id.end);
        // Projekt und -beschreibung setzen
        this.project = (AutoCompleteTextView) (this.entry.findViewById(R.id.edit_text_tracking_project));
        this.project.setThreshold(0);
        this.description = (AutoCompleteTextView) (this.entry.findViewById(R.id.edit_text_tracking_description));
        this.description.setThreshold(0);

        this.duration = (TextView) (this.entry.findViewById(R.id.text_tracking_duration));
        // Anfangs- und Enddatum
        this.beginDate = (TextView) (begin.findViewById(R.id.text_intervalDate));
        this.endDate = (TextView) (end.findViewById(R.id.text_intervalDate));
        // Anfangs- und Endzeit
        this.beginTime = (TextView) (begin.findViewById(R.id.text_intervalTime));
        this.endTime = (TextView) (end.findViewById(R.id.text_intervalTime));

        setTags(tag);
        setDataToViews();
    }

    /**
     * Setzt bei jedem Feld des Tracking-Eintrages die Nummer in der Liste.
     * Die Nummer wird benötigt, um den richtigen Datenbankeintrag nicht suchen zu müssen.
     * @param tag zu setzende Frontend-ID
     */
    public void setTags(int tag) {
        this.project.setTag(tag);
        this.description.setTag(tag);
        this.entry.setTag(tag);
        this.duration.setTag(tag);
        this.beginDate.setTag(tag);
        this.endDate.setTag(tag);
        this.beginTime.setTag(tag);
        this.endTime.setTag(tag);
        this.restartTracking.setTag(tag);
    }

    /**
     * Bringt die Daten aus dem Eintrag zur Anzeige.
     */
    private void setDataToViews() {
        // Projektnamen und -beschreibung setzen
        this.project.setText(this.trackingEntry.getProject());
        this.description.setText(this.trackingEntry.getDescription());
        setTimestampsToViews();
    }

    /**
     * Setzt die TextViews für Dauer, Start- und Endzeitpunkt auf die aktuellen Werte.
     */
    public void setTimestampsToViews() {
        // Startdaumt und -Zeit setzen
        this.beginDate.setText(DateFormat.getDateFormat(this.parent).format(this.trackingEntry.getBegin()
                .getTime()));
        this.beginTime.setText(DateFormat.getTimeFormat(this.parent).format(this.trackingEntry.getBegin().getTime()));

        // Enddatum setzen, wenn vorhanden
        Calendar end = this.trackingEntry.getEnd();
        if(end != null) {
            this.endDate.setText(DateFormat.getDateFormat(this.parent).format(end.getTime()));
            this.endTime.setText(DateFormat.getTimeFormat(this.parent).format(end.getTime()));
        }

        // Arbeitsdauer setzen
        this.duration.setText(
                formatTime(this.trackingEntry.getDuration(), true)
        );
    }
}
