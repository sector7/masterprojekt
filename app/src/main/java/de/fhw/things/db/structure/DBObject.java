package de.fhw.things.db.structure;

/**
 * Schnittstelle für Datenbankstrukturobjekte für SQLite
 */
interface DBObject {

    /**
     * @return SQLite-Definition der SQLite-Struktur des Objekts
     */
    String getSQLiteDefinition();
}
