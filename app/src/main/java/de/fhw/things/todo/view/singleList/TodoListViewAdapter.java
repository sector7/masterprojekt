package de.fhw.things.todo.view.singleList;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import de.fhw.things.R;
import de.fhw.things.todo.data.TodoListWrapper;
import de.fhw.things.todo.data.beans.TodoEntry;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static de.fhw.things.logger.Log.TODO_LOGGER;

/**
 * Adapter zur Anzeige einer Todoliste.
 * Initialisiert Listener und aktualisiert die Anzeige.
 * Greift zur Manipulation der TodoListe sowie der Datenbankarbeit auf den
 * TodoListWrapper zurück.
 */
public class TodoListViewAdapter extends ArrayAdapter<TodoEntry> {

    /** Manipuliert die Einträge der TodoListe und sorgt für eine konsistente Datenbank */
    private final TodoListWrapper todoListWrapper;
    /** View */
    private final View view;
    /** TodoListFragment, in dem dieser Adapter enthalten ist. */
    private final TodoListFragment parentFragment;

    /**
     * Initialisiert den Adapter für die TodoListe.
     * @param context            Kontext
     * @param view               View
     * @param textViewResourceId id
     * @param wrapper            Wrapper zur Arbeit mit den TodoListen.
     */
    public TodoListViewAdapter(FragmentActivity context, View view, int textViewResourceId, TodoListWrapper wrapper, TodoListFragment parentFragment) {
        super(context, textViewResourceId, wrapper.getEntries());
        this.todoListWrapper = wrapper;
        this.view = view;
        this.parentFragment = parentFragment;
    }

    /**
     * Liefert die Id eines Eintrages an der angegebenen Position.
     * @param position Position
     * @return ID
     */
    @Override
    public long getItemId(int position) {
        Integer id = (position > 0 && position < this.todoListWrapper.getEntries().size())
                ? getItem(position).getPosition()
                : null;
        return (id != null) ? id : -1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    /**
     * Liefert einen Listeneintrag an der gegebenen Position.
     * @param position Position
     * @param convertView View
     * @param parent Parent View
     * @return View des Listeneintrags
     */
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Das Layout auf einen Listeneintrag anwenden
        LayoutInflater inflator = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflator.inflate(R.layout.layout_todolist_entry, parent, false);

        final TodoEntryHolder entry = initTodoListEntry(position, convertView);
        setListeners(entry, position);

        return convertView;
    }

    /**
     * Initialisiert ein TodolistEntry - liest die verschiedenen Views aus und
     * setzt Inhalt, Werte und Aussehen.
     * @param position Position des Entries in der Liste
     * @param convertView View
     * @return initialisierter Eintrag
     */
    @NonNull
    private TodoEntryHolder initTodoListEntry(int position, View convertView) {
        final TodoEntryHolder entry = new TodoEntryHolder();
        // Die Views für alle Felder in den Holder setzen
        entry.delete = (ImageButton) convertView.findViewById(R.id.text_delete);
        entry.done = (CheckBox) convertView.findViewById(R.id.cb_done);
        entry.editText = (EditText) convertView.findViewById(R.id.edit_text);
        entry.prioritized = (Button) convertView.findViewById(R.id.text_prioritized);

        // Die Daten der Views in den Holder setzen
        entry.id = this.todoListWrapper.get(position).getID();
        String text = this.todoListWrapper.get(position).getText();
        entry.editText.setText(text);
        entry.done.setChecked(this.todoListWrapper.get(position).isDone());
        togglePrioritizedView(entry.prioritized, this.todoListWrapper.get(position).isPrioritized());

        return entry;
    }

    /**
     * Setzt die Farbe des "!"-Feldes under togglet die Anzeige je nach Priorisierung.
     * @param text Textfeld
     * @param prioritized Prisorisierung
     */
    private void togglePrioritizedView(TextView text, Boolean prioritized) {
        if (prioritized) {
            text.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
        } else {
            text.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
        }
    }


    /**
     * Setzt die verschiedenen Listener zur Behandlung der User-Aktionen.
     * @param entry  TodoEintrag
     * @param position Position in der Liste
     */
    private void setListeners(TodoEntryHolder entry, final int position) {
        setEditTextListener(entry, position);
        setCheckedListener(entry, position);
        setPrioritizeListener(entry, position);
        setDeleteListener(entry, position);
    }

    /**
     * Setzt den Listener zur Bearbeitung des Texts.
     * @param entryHolder  TodoEintrag
     * @param position Position in der Liste
     */
    private void setEditTextListener(final TodoEntryHolder entryHolder, final int position) {
        entryHolder.editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (TodoListViewAdapter.this.todoListWrapper.getEntries().size() > position) {
                    TodoEntry entry = TodoListViewAdapter.this.todoListWrapper.get(position).copy();
                    if (!hasFocus) {
                        String currentText = ((EditText) v).getText().toString();
                        TodoListViewAdapter.this.todoListWrapper.updateEntryText(entry, currentText);
                    }
                    toggleEditSymbols(entryHolder, entry, hasFocus);
                }
            }
        });
        entryHolder.editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // Bei "Fertig" Fokus von EditText lösen und Tastatur verstecken
                    hideSoftkeyboardAndClearFocus(entryHolder);
                }
                return false;
            }
        });
    }

    /**
     * Versteckt das Softkeyboard und löst den Fokus von etwaigen EditTexts.
     * @param entryHolder TodoEintrag
     */
    private void hideSoftkeyboardAndClearFocus(TodoEntryHolder entryHolder) {
        this.view.clearFocus();
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(entryHolder.editText.getWindowToken(), 0);
    }

    /**
     * Blendet Schaltflächen zur Bearbeitung eines TodoEintrags ein oder aus.
     * @param entryHolder TodoEntry
     * @param show Anzeige-Einstellung
     */
    private void toggleEditSymbols(TodoEntryHolder entryHolder, TodoEntry entry, boolean show) {
        if (show) {
            TODO_LOGGER.v("Zeige Edit-Symbole");
            if (entry.isPrioritized())
                entryHolder.prioritized.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
            else
                entryHolder.prioritized.setTextColor(ContextCompat.getColor(getContext(), R.color.grey));
            entryHolder.prioritized.setVisibility(View.VISIBLE);
            entryHolder.delete.setVisibility(View.VISIBLE);
            entryHolder.done.setVisibility(View.GONE);
        } else {
            TODO_LOGGER.v("Verberge Edit-Symbole");
            if (entry.isPrioritized())
                entryHolder.prioritized.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
            else
                entryHolder.prioritized.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            entryHolder.delete.setVisibility(View.GONE);
            entryHolder.done.setVisibility(View.VISIBLE);
        }

    }

    /**
     * Setzt den Listener zur Bearbeitung des checked-Status.
     * @param entry  TodoEintrag
     * @param position Position in der Liste
     */
    private void setCheckedListener(final TodoEntryHolder entry, final int position) {
        entry.done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftkeyboardAndClearFocus(entry);
                CheckBox checkbox = (CheckBox) v;
                TodoListViewAdapter.this.todoListWrapper.updateEntryDone(TodoListViewAdapter.this.todoListWrapper.get(position).copy(), checkbox.isChecked());
            }
        });
    }

    /**
     * Setzt den Listener zur (De-)Priorisierung von TodoEinträgen.
     * @param entryHolder  TodoEintrag
     * @param position Position in der Liste
     */
    private void setPrioritizeListener(final TodoEntryHolder entryHolder, final int position) {
        entryHolder.prioritized.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftkeyboardAndClearFocus(entryHolder);
                TodoEntry entry = TodoListViewAdapter.this.todoListWrapper.getEntry(position).copy();
                TodoListViewAdapter.this.todoListWrapper.toggleEntryPriority(entry);
                togglePrioritizedView(entryHolder.prioritized, entry.isPrioritized());
                notifyDataSetChanged();
            }
        });
    }

    /**
     * Setzt den Listener zum Löschen eines TodoEintrags.
     * @param entryHolder  TodoEintrag
     * @param position Position in der Liste
     */
    private void setDeleteListener(final TodoEntryHolder entryHolder, final int position) {
        entryHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Löscht einen Eintrag und aktualisiert die
                // Anzeige, Datenbank und die Adapter-Ids
                hideSoftkeyboardAndClearFocus(entryHolder);
                TodoEntry entry = TodoListViewAdapter.this.todoListWrapper.getEntry(position).copy();
                TodoListViewAdapter.this.todoListWrapper.removeEntry(entry);
                TodoListViewAdapter.this.parentFragment.resetAdapter();
            }
        });
    }

}
