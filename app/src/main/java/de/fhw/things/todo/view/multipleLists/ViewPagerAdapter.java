package de.fhw.things.todo.view.multipleLists;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Pair;

import java.util.List;

import de.fhw.things.todo.view.singleList.TodoListFragment;

/**
 * Adapter zur Verwaltung der Tabs. Wird genutzt, um die Tab-Titel zu speichern und
 * ihre Breite anhand der Anzahl zu berechnen.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    /** Zuordnung von Ids und Titel der Tabs */
    private List<Pair<Long, String>> mTabs;

    /**
     * Speichert die benötigten Variablen zwischen.
     * @param fm FragmentManager
     * @param tabs Zuordnung von Ids und Titel der Tabs
     */
    public ViewPagerAdapter(FragmentManager fm, List<Pair<Long, String>> tabs) {
        super(fm);
        this.mTabs = tabs;
    }

    /**
     * Liefert das Fragment (beinhaltet eine TodoListe) einer gegebenen Tab-Position
     * @param position Tab-Position
     * @return TodoListen Fragment
     */
    @Override
    public Fragment getItem(int position) {
        return TodoListFragment.newInstance(this.mTabs.get(position).first.intValue());
    }

    /**
     * Liefert den Titel des Fragmentes einer gegebenen Tab-Position. Dieser String
     * wird auf dem Tab angezeigt.
     * @param position Tab-Position
     * @return Tab-Titel
     */
    @Override
    public CharSequence getPageTitle(int position) {
        return this.mTabs.get(position).second;
    }

    /**
     * Liefert die Anzahl der Tabs. Wird genutzt um die Breite eines Tabs zu berechnen
     * @return Anahl der Tabs.
     */
    @Override
    public int getCount() {
        return this.mTabs.size();
    }

}