package de.fhw.things.db.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import de.fhw.things.db.structure.DB;
import de.fhw.things.db.structure.DBColumn;
import de.fhw.things.db.structure.DBConstraint;
import de.fhw.things.db.structure.DBTable;
import de.fhw.things.db.structure.TableProperties;
import de.fhw.things.logger.Log;
import de.fhw.things.todo.data.beans.TodoEntry;
import de.fhw.things.todo.data.beans.TodoList;

import static de.fhw.things.db.structure.DB.getDB;

/** Datenbankobjekt zur Arbeit auf einer TodoListe. */
public final class TodoListDBAdapter extends TableAdapter<TodoList, TodoListDBAdapter.COLUMN> {

    /** Statisches Objekt zum Auslesen von Tabellen-Meta-Informationen */
    public static final TableProperties<COLUMN> P = new TodoProperties();

    /** statischer Adapter */
    private static TodoListDBAdapter TODOLIST_DB_ADAPTER;

    /**
     * Konstruktor.
     *
     * @param db Datenbankanbindung
     */
    private TodoListDBAdapter(DB db) {
        super(db, P.table);
    }

    /**
     * @return statischer DB Adapter
     */
    public static TodoListDBAdapter getTodoListDBAdapter(){
        if (TODOLIST_DB_ADAPTER == null) {
            TODOLIST_DB_ADAPTER = new TodoListDBAdapter(getDB());
        }
        return TODOLIST_DB_ADAPTER;
    }

    /**
     * @param context Kontext
     * @return statischer DB Adapter
     */
    public static TodoListDBAdapter getTodoListDBAdapter(Context context){
        if(TODOLIST_DB_ADAPTER == null){
            TODOLIST_DB_ADAPTER = new TodoListDBAdapter(getDB(context));
        }
        return TODOLIST_DB_ADAPTER;
    }

    /**
     * Statische Methoden zum Einfügen einer TodoListe inklusive ihrer Einträge in eine Datenbank.
     * Dabei wird die ID der TodoListe entsprechend gesetzt.
     *
     * @param db       Datenbank, in die die TodoListe eingetragen wird
     * @param todoList TodoListe, die mit ihren Einträgen gespeichert wird
     * @return ID der gespeicherten TodoListe oder -1 bei Fehlschlag
     */
    public static long insert(SQLiteDatabase db, TodoList todoList) {
        return TableAdapter.insert(db, P.tableName, todoList, staticContentValues(todoList));
    }

    /**
     * Liest eine TodoListe und ihre Einträge anhand einer ID aus der Datenbank
     *
     * @param db Datenbank, die zum Lesen genutzt wird
     * @param id ID des gesuchten Datenbankeintrags
     * @return TodoListen-Objekt oder <tt>null</tt>, wenn unter der ID kein Eintrag vorhanden ist
     */
    public static TodoList byID(SQLiteDatabase db, long id) {
        final Cursor c = db.query(P.tableName, P.table.getColumnNames(), "_id=" + id, null, null, null, null, "1");
        if (!c.moveToFirst()) {
            c.close();
            Log.DB_LOGGER.w("TodoListe mit ID " + id + " nicht in der Datenbank vorhanden!");
            return null;
        }
        final TodoList list = fromCursor(db, c, null);
        c.close();
        return list;
    }

    /**
     * Erstellt eine TodoListe anhand einer Datenbank-Zeile
     *
     * @param db    Datenbankverbindung zur Auflösung etwaiger Fremdreferenzen
     * @param c     Cursor auf die auszulesende Datenbankzeile
     * @param child schon initialisierte Fremdreferenz, oder <tt>null</tt>, falls diese instanziert werden muss
     * @return TodoList-Objekt mit den aus der Datenbank gelesenen Werten
     */
    private static TodoList fromCursor(SQLiteDatabase db, Cursor c, @Nullable TodoEntry child) {
        TodoList list = new TodoList(c.getString(c.getColumnIndex(P.name(COLUMN.TITLE))));
        list.setID(c.getLong(c.getColumnIndex(P.name(COLUMN.ID))));
        list.setEntries(TodoEntryDBAdapter.byTodoList(db, list, child));
        return list;
    }

    /**
     * Aktualisiert eine TodoListe in der Datenbank
     *
     * @param db       Datenbankverbindung
     * @param todoList Zu aktualisierende TodoListe
     */
    @SuppressWarnings("ConstantConditions")
    public static void update(SQLiteDatabase db, TodoList todoList) {
        TableAdapter.update(db, P.tableName, todoList.getID(), staticContentValues(todoList));
    }

    /**
     * Bildet die Informationen einer TodoListe auf eine Datenbank-kompatible Datenstruktur ab
     *
     * @param todoList Abzubildende TodoListe
     * @return Schlüssel-Wert Paare, die in die Datenbank geschrieben werden können
     */
    private static ContentValues staticContentValues(TodoList todoList) {
        final ContentValues values = new ContentValues(COLUMN.values().length - 1);
        values.put(P.name(COLUMN.TITLE), todoList.getTitle());
        return values;
    }

    /**
     * Liefert eine Übersicht aller TodoListen mit ihren jeweiligen IDs und ihren Namen.
     *
     * @param db Datenbankverbindung
     * @return Sortierte Liste mit IDs und den Namen der zugehörigen TodoListe
     */
    public static List<Pair<Long, String>> getAllTodoLists(SQLiteDatabase db) {
        List<Pair<Long, String>> list = new ArrayList<>();
        Cursor c = db.query(P.tableName, new String[]{P.name(COLUMN.ID), P.name(COLUMN.TITLE)},
                null, null, null, null, P.name(COLUMN.ID));
        while (c.moveToNext()) {
            list.add(Pair.create(c.getLong(0), c.getString(1)));
        }
        c.close();
        return Collections.unmodifiableList(list);
    }

    /**
     * Setzt den "erledigt"-Status aller Einträge einer bestimmten TodoListe zurück
     *
     * @param db       Datenbankverbindung
     * @param todoList TodoListe, deren Einträge zurückgesetzt werden
     */
    @SuppressWarnings("ConstantConditions")
    public static void resetEntries(SQLiteDatabase db, TodoList todoList) {
        resetEntries(db, todoList.getID());
        for (TodoEntry entry : todoList.getEntries()) {
            entry.setDone(false);
        }
    }

    /**
     * Setzt den "erledigt"-Status aller Einträge einer bestimmten TodoListe zurück
     *
     * @param db       Datenbankverbindung
     * @param todoList TodoListe, deren Einträge zurückgesetzt werden
     */
    public static void resetEntries(SQLiteDatabase db, long todoList) {
        final ContentValues fields = new ContentValues(1);
        fields.put(TodoEntryDBAdapter.P.name(TodoEntryDBAdapter.COLUMN.DONE), false);
        updateAllEntries(db, todoList, fields);
    }

    /**
     * Setzt den "erledigt"-Status aller Einträge einer bestimmten TodoListe zurück
     *
     * @param db       Datenbankverbindung
     * @param todoList ID der TodoListe, deren Einträge zurückgesetzt werden
     * @param fields   Felder, deren Werte auf einen Neuen Wert aktualisiert werden sollen.
     */
    private static void updateAllEntries(SQLiteDatabase db, long todoList, ContentValues fields) {
        db.update(TodoEntryDBAdapter.P.tableName, fields,
                TodoEntryDBAdapter.P.name(TodoEntryDBAdapter.COLUMN.TODO_LIST) + "=" + todoList, null);
    }

    @Override
    public TodoList byID(long id) {
        final SQLiteDatabase database = this.db.getReadableDatabase();
        TodoList list = byID(database, id);
        database.close();
        return list;
    }

    @NonNull
    @Override
    ContentValues getContentValues(@NonNull TodoList obj) {
        return staticContentValues(obj);
    }

    /**
     * Liefert eine Übersicht aller TodoListen mit ihren jeweiligen IDs und ihren Namen.
     *
     * @return Sortierte Liste mit IDs und den Namen der zugehörigen TodoListe
     */
    public List<Pair<Long, String>> getAllTodoLists() {
        final SQLiteDatabase database = this.db.getReadableDatabase();
        List<Pair<Long, String>> lists = getAllTodoLists(database);
        database.close();
        return lists;
    }

    /**
     * Setzt den "erledigt"-Status aller Einträge einer bestimmten TodoListe zurück
     *
     * @param todoList TodoListe, deren Einträge zurückgesetzt werden
     */
    public void resetEntries(TodoList todoList) {
        final SQLiteDatabase database = this.db.getWritableDatabase();
        resetEntries(database, todoList);
        database.close();
    }

    /**
     * Setzt den "erledigt"-Status aller Einträge einer bestimmten TodoListe zurück
     *
     * @param todoList ID der TodoListe, deren Einträge zurückgesetzt werden
     */
    public void resetEntries(long todoList) {
        final SQLiteDatabase database = this.db.getWritableDatabase();
        resetEntries(database, todoList);
        database.close();
    }

    /** Aufzählung der Spalten */
    public enum COLUMN {
        ID, // obligatorische ID-Spalte
        TITLE // Name der TodoListe
    }

    /** Eigenschaften-Klasse für TodoListen */
    private static final class TodoProperties extends TableProperties<COLUMN> {
        @Override
        protected Map<COLUMN, DBColumn> fillColumnMap() {
            EnumMap<COLUMN, DBColumn> columns = new EnumMap<>(COLUMN.class);
            columns.put(COLUMN.ID, DBColumn.COLUMN_ID);
            columns.put(COLUMN.TITLE, new DBColumn("title", DBColumn.TEXT, DBConstraint.NOT_NULL));
            return Collections.unmodifiableMap(columns);
        }

        @Override
        public String getTableName() {
            return "todoList";
        }

        @Override
        protected DBTable<COLUMN> initTable() {
            return new DBTable<COLUMN>(this) {
                @Override
                public void create(SQLiteDatabase db) {
                    super.create(db);
                    TodoListDBAdapter.insert(db, new TodoList("Todo"));
                }
            };
        }
    }
}
