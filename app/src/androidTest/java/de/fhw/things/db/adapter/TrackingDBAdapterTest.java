package de.fhw.things.db.adapter;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import org.junit.After;
import org.junit.Before;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import de.fhw.things.tracking.data.TrackingEntry;

/**
 * Testklasse zur Abdeckung des Todolist-Adapters.
 */
public class TrackingDBAdapterTest extends AndroidTestCase {

    /** Testee */
    TrackingDBAdapter adapter;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        RenamingDelegatingContext context = new RenamingDelegatingContext(getContext(), "test_");
        this.adapter = TrackingDBAdapter.getTrackingDBAdapter(context);
    }

    @After
    public void tearDown() {
        for (TrackingEntry entry : this.adapter.getAll()) {
            this.adapter.delete(entry);
        }
    }

    public void testInsert() throws Exception {
        // Given
        TrackingEntry entry = new TrackingEntry();
        // When
        long id = this.adapter.insert(entry);
        // Then
        assertEquals(Long.valueOf(id), entry.getID());
        assertNotNull(entry.getBegin());
        assertTrue(new GregorianCalendar().after(entry.getBegin()));
    }

    public void testDelete() throws Exception {
        // Given
        Calendar c1 = new GregorianCalendar(), c2 = new GregorianCalendar();
        TrackingEntry entry = new TrackingEntry(c1, c2);
        long id = this.adapter.insert(entry);
        assertTrue(this.adapter.byID(id) != null);
        // When
        boolean success = this.adapter.delete(entry);
        // Then
        assertTrue(success);
        assertTrue(entry.getID() == null);
        assertTrue(this.adapter.byID(id) == null);
    }

    public void testDelete_nonexistingEntry() {
        // Given
        TrackingEntry entry = new TrackingEntry(
                null, new GregorianCalendar(), new GregorianCalendar(),
                "Projekt", "Beschreibung"
        );
        // When / Then
        assertFalse(this.adapter.delete(entry));
    }

    public void testDelete_nonexistingId() {
        // Given
        assertTrue(this.adapter.byID(123l) == null);
        // When
        boolean success = this.adapter.delete(123l);
        // Then
        assertFalse(success);
    }

    @SuppressWarnings("ConstantConditions")
    public void testByID() throws Exception {
        // Given
        Calendar c1 = new GregorianCalendar(), c2 = new GregorianCalendar();
        c2.add(Calendar.MINUTE, 42);
        TrackingEntry entry = new TrackingEntry(
                null, c1, c2,
                "Projekt", "Beschreibung"
        );
        // When
        Long id = this.adapter.insert(entry);
        // Then
        TrackingEntry dbEntry = this.adapter.byID(id);
        assertNotSame(dbEntry, entry);
        assertEquals(entry.getID(), dbEntry.getID());
        assertEquals(id, dbEntry.getID());
        assertEquals(c1, dbEntry.getBegin());
        assertEquals(c2, dbEntry.getEnd());
        assertEquals("Projekt", dbEntry.getProject());
        assertEquals("Beschreibung", dbEntry.getDescription());
    }

    public void testGetAllByProject() throws Exception {
        // Given
        insertSampleEntries(Arrays.asList("Datenbanken", "Berechenbarkeit und Komplexität",
                "Verteilte Systeme", "Masterprojekt"));

        // When/Then
        assertEquals(100, this.adapter.getAllByProject(null).size());
        assertEquals(100, this.adapter.getAllByProject("Datenbanken").size() +
                this.adapter.getAllByProject("Berechenbarkeit und Komplexität").size() +
                this.adapter.getAllByProject("Verteilte Systeme").size() +
                this.adapter.getAllByProject("Masterprojekt").size());
    }

    private void insertSampleEntries(List<String> projects) {
        for (int i = 0; i < 100; i++) {
            Calendar begin = Calendar.getInstance();
            Calendar end = Calendar.getInstance();
            begin.add(Calendar.HOUR_OF_DAY, Math.round((float) Math.random() * 7));
            begin.add(Calendar.MINUTE, Math.round((float) Math.random() * 59));
            end.setTimeInMillis(begin.getTimeInMillis());
            end.add(Calendar.HOUR_OF_DAY, Math.round((float) Math.random() * 7));
            end.add(Calendar.MINUTE, Math.round((float) Math.random() * 59));
            Collections.shuffle(projects);
            this.adapter.insert(new TrackingEntry(null, begin, end, projects.get(0), null));
        }
    }

    public void testGetRunningEntry() throws Exception {
        // Given
        assertNull(this.adapter.getRunningEntry());
        long id = this.adapter.insert(new TrackingEntry());
        // When
        TrackingEntry entry = this.adapter.getRunningEntry();
        // Then
        assertNotNull(entry);
        assertEquals(Long.valueOf(id), entry.getID());
    }

    public void testGetAllProjectNames() throws Exception {
        // Given
        insertSampleEntries(Arrays.asList("Programmstrukturen", "Analysis",
                "Informationstechnik", "Betriebswirtschaftslehre"));
        // When
        SortedSet<String> names = this.adapter.getAllProjectNames();
        // Then
        assertEquals(new TreeSet<>(Arrays.asList("Analysis", "Betriebswirtschaftslehre", "Informationstechnik",
                "Programmstrukturen")), names);
    }

    public void testGetTrackedTime() throws Exception {
        // Given
        Calendar begin = GregorianCalendar.getInstance();
        Calendar end = GregorianCalendar.getInstance();
        end.setTimeInMillis(begin.getTimeInMillis() + 50);
        for (int i = 1; i <= 10; i++)
            this.adapter.insert(new TrackingEntry(null, begin, end, "p" + i % 3, null));

        this.adapter.insert(new TrackingEntry(null, begin, end, "", null));
        this.adapter.insert(new TrackingEntry(null, begin, end, null, null));

        // When
        long timeForAll = this.adapter.getTrackedTime();
        long timeForEmptyProject = this.adapter.getTrackedTime("");
        long[] timeForP = new long[]{
                this.adapter.getTrackedTime("p0"),
                this.adapter.getTrackedTime("p1"),
                this.adapter.getTrackedTime("p2")};

        // Then
        assertEquals(50 * 12, timeForAll);
        assertEquals(50, timeForEmptyProject);
        assertEquals(50 * 3, timeForP[0]);
        assertEquals(50 * 4, timeForP[1]);
        assertEquals(50 * 3, timeForP[2]);
    }
}