package de.fhw.things.todo;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.common.view.SlidingTabLayout;

import java.util.List;

import de.fhw.things.R;
import de.fhw.things.commons.UIFunctions;
import de.fhw.things.db.adapter.TodoListDBAdapter;
import de.fhw.things.db.structure.DB;
import de.fhw.things.todo.data.beans.TodoList;
import de.fhw.things.todo.view.multipleLists.ViewPagerAdapter;
import de.fhw.things.tracking.TrackingActivity;
import de.fhw.things.trackinghistory.TrackingHistoryActivity;

import static de.fhw.things.commons.UIFunctions.hideSoftkeyboardAndRemoveFocus;
import static de.fhw.things.commons.UIFunctions.switchActivatedActivityButtons;
import static de.fhw.things.db.adapter.TodoListDBAdapter.getTodoListDBAdapter;

/**
 * Hauptklasse für Ansicht von Aufgabenlisten.
 */
public class TodoActivity extends AppCompatActivity {
    /** Grundlegende Tab-Ansicht der verschiedenen Todolisten */
    private ViewPager pager;
    /** Spezielle Darstellung der Tabs */
    private SlidingTabLayout tabs;
    /** Toolbar zum Bearbeiten der Todoliste */
    private Toolbar editTodoListToolbar;
    /** Direkter Zugriff auf Element in Toolbar, um Namen der Todoliste direkt zu bearbeiten  */
    private MenuItem editTodolistnameButton;
    /** Id und Name der Todolisten in einem Tab */
    private List<Pair<Long, String>> currentTabs;
    /** Id des aktuell angezeigten Tabs */
    private int activeTabNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);

        this.tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        this.tabs.setDistributeEvenly(true); // sorgt für gleichbreite Tabs

        this.editTodoListToolbar = (Toolbar) findViewById(R.id.toolbar_edittodolist);
        this.setSupportActionBar(this.editTodoListToolbar);

        // ViewPager View zuweisen und Adapter setzen
        this.pager = (ViewPager) findViewById(R.id.pager);
        setPageChangeListener(this.pager);

        buildTabs(false);
        switchActivatedActivityButtons(this, this.getClass());
    }

    /**
     * Fügt alle Aktionsknöpfe zur Toolbar hinzu.
     * @param menu Optionen-Menu
     * @return true: um Menu anzuzeigen, false: wird nicht angezeigt
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_todo, menu);

        // Listener definieren
        MenuItemCompat.OnActionExpandListener expandListener = new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                String editTodolistName = ((EditText) item.getActionView().findViewById(R.id.edit_todolistname)).getText().toString();
                if (!TodoActivity.this.currentTabs.get(TodoActivity.this.activeTabNumber).second.equals(editTodolistName))
                    setTodolistTitle(TodoActivity.this.currentTabs.get(TodoActivity.this.activeTabNumber).first, editTodolistName);
                setItemsVisibility(menu, item, true);
                hideSoftkeyboardAndRemoveFocus(getApplicationContext(), findViewById(R.id.editTextNewTodo));
                return true;  // sorgt für Ausblenden der ActionView
            }
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                setItemsVisibility(menu, item, false);
                EditText editTodolistName = (EditText) item.getActionView().findViewById(R.id.edit_todolistname);
                editTodolistName.setText(TodoActivity.this.currentTabs.get(TodoActivity.this.activeTabNumber).second);
                showSoftkeyboardAndSetFocus(editTodolistName);
                return true;  // sorgt für einblenden der ActionView
            }
        };


        // Holen des MenuItems für die ActionView und Listener zuweisen
        this.editTodolistnameButton = menu.findItem(R.id.action_edit_todolist);
        MenuItemCompat.setOnActionExpandListener(this.editTodolistnameButton, expandListener);

        // Setzen eines Listeners zum Beenden des Bearbeiten-Modus beim Druck auf Fertig.
        ((EditText)this.editTodolistnameButton.getActionView().findViewById(R.id.edit_todolistname))
                .setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE)
                            editTodolistnameButton.collapseActionView();
                        return false;
                    }
                });

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Legt Aktionen für die Aktionsknöpfe der Toolbar fest
     * @param item Gewählter Knopf
     * @return boolean false: Klick an unterliegende Komponenten weiterleiten true: Klick hier abfangen
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Long activeTodolistID = this.currentTabs.get(this.activeTabNumber).first;
        switch (item.getItemId()) {
            // Löschen der gerade aktivierten Todoliste
            case R.id.action_delete_todolist:
                deleteTodolist(activeTodolistID);
                Toast.makeText(this, R.string.event_todolist_deleted, Toast.LENGTH_SHORT)
                        .show();
                break;
            // Neustarten aller Einträge der aktivierten Todoliste
            case R.id.action_reset_todolist:
                resetTodolist(activeTodolistID);
                Toast.makeText(this, R.string.event_todolist_reset, Toast.LENGTH_SHORT)
                        .show();

                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Setzt die Sichtbarkeit aller Items mit einer (möglichen) Ausnahme.
     * @param menu Menü, dessen Items versteckt / angezeigt werden sollen
     * @param exception Ausnahme
     * @param visible Gewünschte Sichtbarkeit
     */
    private void setItemsVisibility(final Menu menu, final MenuItem exception,
                                    final boolean visible) {
        for (int i = 0; i < menu.size(); ++i) {
            MenuItem item = menu.getItem(i);
            if (item != exception)
                item.setVisible(visible);
        }
    }

    /**
     * EventListener für das Erstellen einer neuen Todoliste.
     *
     * @param view Aktuelle View (Wird vom Code nicht benötigt)
     */
    public void createNewTodolist(View view) {
        createNewTodolist();
    }

    /**
     * Erzeugt eine neue Todoliste, speichert sie in die Datenbank und akualisiert
     * die Tabs.
     */
    private void createNewTodolist() {
        if (this.editTodoListToolbar.hasExpandedActionView())
            this.editTodoListToolbar.collapseActionView();
        TodoListDBAdapter.insert(DB.getDB(this).getWritableDatabase(),
                new TodoList(this.getString(R.string.string_default_todolist_title)));
        buildTabs(true);
        showToolbar(true);

        this.editTodolistnameButton.expandActionView();
    }

    /**
     * Setzt den 'erledigt'-Status aller Todos der angegebenen Liste auf 'nicht erledigt'.
     * @param id ID der zurückzusetzenden Liste
     */
    private void resetTodolist(long id) {
        getTodoListDBAdapter(this).resetEntries(id);
        buildTabs(false);
    }

    /**
     * Löscht die angegebene Todoliste.
     *
     * @param id ID der zu löschenden Liste
     */
    private void deleteTodolist(Long id){
        getTodoListDBAdapter(this).delete(id);
        // Wenn gerade die letzte Todoliste gelöscht wurde, erstelle eine neue mit einem default-Namen
        if (this.currentTabs.size() == 1) {
            createNewTodolist();
        }
        setPreferencesActiveTabNumber(this.activeTabNumber > 0 ? this.activeTabNumber - 1 : 0);
        buildTabs(false);
    }

    /**
     * Ändert den Titel einer Todoliste in der Datenbank und aktualisiert die Ansicht.
     * @param id ID der anzupassenden Todoliste
     * @param newTitle Neuer Titel
     */
    private void setTodolistTitle(Long id, String newTitle) {
        if(newTitle != null && !"".equals(newTitle)) {
            TodoList currentTodolist = getTodoListDBAdapter(this).byID(id);
            if(currentTodolist != null) {
                currentTodolist.setTitle(newTitle);
                getTodoListDBAdapter(this).update(currentTodolist);
                buildTabs(false);
            }
        }
    }
    /**
     * Lädt die Titel aller Tabs und erstellt die Tableiste. Wird eine neue Todoliste angelegt,
     * kann diese über jumpToLast direkt angezeigt werden.
     *
     * @param jumpToLast Den aktuell letzten Tab anzeigen
     */
    private void buildTabs(boolean jumpToLast) {
        // Aktuelle Tabs aus der Datenbank laden
        this.currentTabs = TodoListDBAdapter.getAllTodoLists(DB.getDB(this).getWritableDatabase());

        // Auswahl des anzuzeigenden Tabs
        if (jumpToLast)
            this.activeTabNumber = this.currentTabs.size() - 1;
        else // Nummer des zuletzt verwendeten Tabs laden
            this.activeTabNumber = (int)(getPreferences(Context.MODE_PRIVATE)
                    .getLong(getString(R.string.last_todolist_number), getResources()
                            .getInteger(R.integer.default_todolist_number)));

        // Erzeugung des ViewPagerAdapters mit einem fragmentManager,
        // der Liste der Tabs und eine Referenz auf diese Activity
        ViewPagerAdapter viewpagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this.currentTabs);
        this.pager.setAdapter(viewpagerAdapter);
        // ViewPager für SlidingTabsLayout setzen
        this.tabs.setViewPager(this.pager);
        this.pager.setCurrentItem(this.activeTabNumber);
        setToolbarTitle(this.activeTabNumber);
    }

    /**
     * Setzen des Listeners für das Abspeichern der zuletzt ausgewählten Todoliste
     * @param viewPager Pager
     */
    private void setPageChangeListener(ViewPager viewPager) {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            /**
             * Wird aufgerufen, wenn eine neue Seite ausgewählt wird. Die Animation muss nicht
             * notwendigerweise abgeschlossen sein.
             *
             * @param position Neue, aufgerufene Position
             * @see <a href="http://developer.android.com/reference/android/support/v4/view/ViewPager.OnPageChangeListener.html#onPageSelected%28int%29">
             *     http://developer.android.com/reference/android/support/v4/view/ViewPager.OnPageChangeListener.html#onPageSelected%28int%29</a>
             */
            @Override
            public void onPageSelected(int position) {
                // Wenn der Titel aktuell bearbeitet wird, diesen abspeichern
                if (isToolbarShown() && TodoActivity.this.editTodoListToolbar.hasExpandedActionView())
                    TodoActivity.this.editTodoListToolbar.collapseActionView();
                // Ausgewählte Todoliste als zuletzt verwendete speichern
                setPreferencesActiveTabNumber(position);
                TodoActivity.this.activeTabNumber = position;
                setToolbarTitle(TodoActivity.this.activeTabNumber);
                TodoActivity.this.pager.setCurrentItem(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // Keine Aktion definiert
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // Keine Aktion definiert
            }
        });
    }

    /**
     * Aktualisiert die zuletzt geladene Todoliste in den Preferences.
     * @param tabNumber Zu speichernde Nummer des Tabs
     */
    private void setPreferencesActiveTabNumber (int tabNumber) {
        getPreferences(Context.MODE_PRIVATE)
                .edit()
                .putLong(getString(R.string.last_todolist_number), tabNumber)
                .commit();
    }

    /**
     * Gibt zurück, ob die Toolbar gerade eingeblendet ist
     * @return Sichtbarkeit der Toolbar
     */
    public boolean isToolbarShown() {
        return this.editTodoListToolbar.getVisibility() == View.VISIBLE;
    }

    /**
     * Blendet die Toolbar ein und aus.
     * @param show Anzeige-Einstellung
     */
    public void showToolbar(boolean show) {
        if(getSupportActionBar() != null)
            if(show)
                getSupportActionBar().show();
            else
                getSupportActionBar().hide();
    }

    /**
     * Setzt den Titel und Untertitel der Toolbar
     * @param currentTabNumber Aktuell ausgewählter Tab.
     */
    @SuppressWarnings("ConstantConditions")
    private void setToolbarTitle(int currentTabNumber) {
        getTodoListDBAdapter().byID(this.currentTabs.get(currentTabNumber).first).getSize();
        setToolbarTitle(this.currentTabs.get(currentTabNumber).second);
    }

    /**
     * Setzt den Titel und Untertitel der Toolbar.
     * @param title Titel der Toolbar
     */
    private void setToolbarTitle(String title) {
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setSubtitle("");
        }
    }

    /**
     * Zeigt das Softkeyboard und setzt den Fokus auf das gegebene EditText.
     * @param edit Zu fokussierendes EditText
     */
    private void showSoftkeyboardAndSetFocus(EditText edit) {
        edit.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
        edit.setSelection(edit.getText().length());
    }

    /**
     * Liefert die Nummer des gerade aktiven Tabs.
     * @return Aktiver Tab
     */
    public int getActiveTabNumber () {
        return this.activeTabNumber;
    }

    /**
     * Startet die Tracking-Activity.
     * @param view Button
     */
    public void startTrackingActivity (View view) {
        new UIFunctions().switchActivity(this, TodoActivity.class, TrackingActivity.class);
    }

    /**
     * Startet die TodoAcitvity.
     * @param view Button
     */
    public void startTodoActiviy(View view) {
        // Hier passiert nichts
    }

    /**
     * Startet die TrackingHistory-Activity
     * @param view Button
     */
    public void startTrackingHistoryActivity(View view) {
        new UIFunctions().switchActivity(this, TodoActivity.class, TrackingHistoryActivity.class);
    }

}
