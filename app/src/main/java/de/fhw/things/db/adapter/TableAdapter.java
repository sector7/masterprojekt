package de.fhw.things.db.adapter;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Calendar;
import java.util.GregorianCalendar;

import de.fhw.things.db.DBEntity;
import de.fhw.things.db.structure.DB;
import de.fhw.things.db.structure.DBTable;

import static de.fhw.things.logger.Log.DB_LOGGER;

/**
 * Klasse zur Verknüpfung der Datenbank mit einer Tabellendefinition, um Methoden zur Manipulation
 * der Daten in der Datenbank bereitzustellen.
 *
 * @param <T> Typ des Objekts, da in der Tabelle gespeichert wird
 * @param <E> Aufzählung der existierenden Tabellenspalten
 */
public abstract class TableAdapter<T extends DBEntity, E extends Enum> {

    /** Datenbankreferenz */
    protected final DB db;

    /** Tabellenreferenz */
    private final DBTable<E> table;

    /**
     * Konstruktor für die Verknüpfung von Datenbank und Tabellendefinition
     *
     * @param db    Datenbankreferenz
     * @param table Tabellendefinition
     */
    protected TableAdapter(DB db, DBTable<E> table) {
        this.db = db;
        this.table = table;
    }

    /**
     * Statische Methode zum Einfügen eines Objekts in die Datenbank.
     *
     * @param db        Datenbank
     * @param tableName Name der Tabelle, in der eingefügt werden soll
     * @param obj       einzufügendes Objekt
     * @return ID, unter der das Objekt gespeichert wurde oder -1 bei Fehler
     */
    protected static long insert(SQLiteDatabase db, String tableName, @NonNull DBEntity obj, @NonNull ContentValues values) {
        DB_LOGGER.v("Insert " + obj + " in Tabelle " + tableName);
        long newID = db.insert(tableName, null, values);
        if (newID >= 0)
            obj.setID(newID);
        else
            DB_LOGGER.e(obj + "Konnte nicht gespeichert werden!");
        return newID;
    }

    /**
     * Löscht einen Eintrag aus der Datenbank auf Grundlage seiner Id.
     *
     * @param db        Datenbankobjekt
     * @param tableName Name der Tabelle
     * @param id        Objekt-ID, das gelöscht werden soll
     * @return True, wenn Objekt gelöscht wurde, False wenn Objekt nicht in DB gefunden
     */
    protected static boolean delete(SQLiteDatabase db, @NonNull String tableName, @Nullable Long id) {
        DB_LOGGER.v("Lösche Objekt mit id " + id + " aus Tabelle " + tableName);
        boolean res = false;
        if (id == null || !(res = db.delete(tableName, "_id=" + id, null) != 0)) {
            DB_LOGGER.e("Fehler beim Löschen von Objekt " + id + ": Objekt nicht gefunden.");
        }
        return res;
    }

    /**
     * Statische Aktualisierung eines Objekts in der Datenbank anhand seiner ID mit den übergebenen Werten.
     *
     * @param db        Datenbankverbindung
     * @param tableName Name der Tabelle
     * @param id        ID des zu aktualisierenden Objekts
     * @param values    Schlüssel-Wert-Paare, die zu aktualisieren sind
     */
    protected static void update(SQLiteDatabase db, @NonNull String tableName, long id, @NonNull ContentValues values) {
        DB_LOGGER.v("Update Objekt mit id " + id + " aus Tabelle " + tableName);
        if (db.update(tableName, values, "_id=" + id, null) == 0) {
            DB_LOGGER.e("Objekt-ID " + id + " in Tabelle " + tableName + " bei Aktualisierung nicht vorhanden");
        }
    }

    /**
     * Statische Aktualisierung eines Objekts, falls es schon in der Datenbank gespeichert war,
     * ansonsten wird es neu eingefügt.
     *
     * @param db        Datenbank
     * @param tableName Name der Tabelle
     * @param obj       Neues bzw. aktualisiertes Objekt
     */
    protected static long upsert(SQLiteDatabase db, @NonNull String tableName, @NonNull DBEntity obj,
                                 @NonNull ContentValues values) {
        DB_LOGGER.v("Upsert Objekt " + obj + " aus Tabelle " + tableName);
        if (obj.getID() != null) {
            update(db, tableName, obj.getID(), values);
        } else {
            insert(db, tableName, obj, values);
        }
        return obj.getID();
    }

    /**
     * Konvertiert einen Zeitpunkt zur Speicherung in einer SQLite Datenbank als Long
     *
     * @param calendar Calendar, der den Zeitpunkt vorgibt
     * @return <tt>null</tt>, wenn der Calendar nicht gesetzt ist, sonst die Anzahl der Millisekunden seit Epochenbeginn
     */
    @Nullable
    protected static Long dbAsLong(@Nullable Calendar calendar) {
        return (calendar == null) ? null : calendar.getTimeInMillis();
    }

    /**
     * Konvertiert einen in der Datenbank gespeicherten Timestamp in ein Calendar-Objekt.
     *
     * @param timeInMillis Timestamp in Millisekunden
     * @return Initialisiertes Calendar-Objekt
     */
    @NonNull
    protected static Calendar dbAsCalendar(long timeInMillis) {
        final GregorianCalendar cal = new GregorianCalendar();
        cal.setTimeInMillis(timeInMillis);
        return cal;
    }

    /**
     * Statische Aktualisierung eines Objekts in der Datenbank anhand seiner ID mit den übergebenen Werten.
     *
     * @param db        Datenbankverbindung
     * @param tableName Name der Tabelle
     * @param obj       Zu aktualisierendes Objekt
     */
    protected void update(SQLiteDatabase db, String tableName, @NonNull T obj) {
        if (obj.getID() != null)
            update(db, tableName, obj.getID(), getContentValues(obj));
        else
            DB_LOGGER.w("Objekt " + obj + " muss vor einem Update in der Datenbank gespeichert werden");
    }

    /**
     * Fügt ein Objekt in die Datenbank ein.
     *
     * @param obj einzufügendes Objekt
     * @return ID, unter der das Objekt gespeichert wurde oder -1 bei Fehler
     */
    public long insert(@NonNull T obj) {
        final SQLiteDatabase database = this.db.getWritableDatabase();
        final long id = insert(database, this.table.getName(), obj, getContentValues(obj));
        database.close();
        return id;
    }

    /**
     * Liefert ein Datenbank-Objekt aus der Tabelle, das anhand seiner ID identifiziert wird
     *
     * @return Das DatenbankObjekt oder <tt>null</tt>, wenn die gesuchte ID sich nicht in der Datenbank befindet
     */
    @Nullable
    public abstract T byID(long id);

    /**
     * Aktualisiert ein bereits in der Datenbank vorhandenes Objekt (anhand seiner ID)
     * mit den Werten des übergebenen Objektes.
     *
     * @param obj Objekt, das die neuen Werte enthält
     */
    @SuppressWarnings("ConstantConditions")
    public void update(@NonNull T obj) {
        final SQLiteDatabase database = this.db.getWritableDatabase();
        update(database, this.table.getName(), obj.getID(), getContentValues(obj));
        database.close();
    }

    /**
     * Aktualisiert ein Objekt in der Datenbank anhand seiner ID mit den übergebenen Werten.
     *
     * @param id     ID des zu aktualisierenden Objekts
     * @param values Schlüssel-Wert-Paare, die zu aktualisieren sind
     */
    public void update(long id, @NonNull ContentValues values) {
        final SQLiteDatabase database = this.db.getWritableDatabase();
        update(database, this.table.getName(), id, values);
        database.close();
    }

    /**
     * Aktualisiert ein Objekt, falls es schon in der Datenbank gespeichert war, ansonsten wird es neu eingefügt.
     *
     * @param obj Neues bzw. aktualisiertes Objekt
     */
    public long upsert(@NonNull T obj) {
        final SQLiteDatabase database = this.db.getWritableDatabase();
        long id = upsert(database, this.table.getName(), obj, getContentValues(obj));
        database.close();
        return id;
    }

    /**
     * Löscht einen Eintrag aus der Datenbank auf Grundlage seiner Id.
     *
     * @param obj Objekt, das gelöscht werden soll
     * @return @return True, wenn Objekt gelöscht wurde, False wenn Objekt nicht in DB gefunden
     */
    public boolean delete(@NonNull T obj) {
        final SQLiteDatabase database = this.db.getWritableDatabase();
        final boolean successful = delete(database, this.table.getName(), obj.getID());
        if (successful) obj.setID(null);
        database.close();
        return successful;
    }

    /**
     * Löscht einen Eintrag aus der Datenbank auf Grundlage seiner Id.
     *
     * @param id ID des Objekts, das gelöscht werden soll
     * @return True, wenn Objekt gelöscht wurde, False wenn Objekt nicht in DB gefunden
     */
    public boolean delete(long id) {
        final SQLiteDatabase database = this.db.getWritableDatabase();
        final boolean successful = delete(database, this.table.getName(), id);
        database.close();
        return successful;
    }

    /**
     * @return Objektinhalt als Key-Value Zuordnungen zum Speichern in der Datenbank.
     */
    @NonNull
    abstract ContentValues getContentValues(@NonNull T obj);
}
