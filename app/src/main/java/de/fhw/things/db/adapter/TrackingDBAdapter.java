package de.fhw.things.db.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import de.fhw.things.db.structure.DB;
import de.fhw.things.db.structure.DBColumn;
import de.fhw.things.db.structure.TableProperties;
import de.fhw.things.logger.Log;
import de.fhw.things.tracking.data.TrackingEntry;

import static de.fhw.things.db.adapter.TrackingDBAdapter.COLUMN.BEGIN;
import static de.fhw.things.db.adapter.TrackingDBAdapter.COLUMN.DESCRIPTION;
import static de.fhw.things.db.adapter.TrackingDBAdapter.COLUMN.END;
import static de.fhw.things.db.adapter.TrackingDBAdapter.COLUMN.ID;
import static de.fhw.things.db.adapter.TrackingDBAdapter.COLUMN.PROJECT;
import static de.fhw.things.db.structure.DB.getDB;
import static de.fhw.things.db.structure.DBColumn.INTEGER;
import static de.fhw.things.db.structure.DBColumn.TEXT;
import static de.fhw.things.db.structure.DBConstraint.NOT_NULL;

/** Datenbankobjekt zur Arbeit auf einer Zeitnahme. */
public final class TrackingDBAdapter extends TableAdapter<TrackingEntry, TrackingDBAdapter.COLUMN> {

    /** Statisches Objekt zum Auslesen von Tabellen-Meta-Informationen */
    public static final TableProperties<COLUMN> P = new TableProperties<COLUMN>() {
        @Override
        protected Map<COLUMN, DBColumn> fillColumnMap() {
            final EnumMap<COLUMN, DBColumn> columnEnumMap = new EnumMap<>(COLUMN.class);
            columnEnumMap.put(ID, DBColumn.COLUMN_ID);
            columnEnumMap.put(BEGIN, new DBColumn("begin", INTEGER, NOT_NULL));
            columnEnumMap.put(END, new DBColumn("end", INTEGER));
            columnEnumMap.put(PROJECT, new DBColumn("project", TEXT));
            columnEnumMap.put(DESCRIPTION, new DBColumn("description", TEXT));
            return Collections.unmodifiableMap(columnEnumMap);
        }

        @Override
        public String getTableName() {
            return "trackingEntry";
        }
    };

    /** statischer Adapter */
    private static TrackingDBAdapter TRACKING_DB_ADAPTER;

    /**
     * Konstruktor.
     *
     * @param db Datenbankanbindung
     */
    private TrackingDBAdapter(DB db) {
        super(db, P.table);
    }

    /**
     * @return statischer DB Adapter
     */
    public static TrackingDBAdapter getTrackingDBAdapter() {
        if (TRACKING_DB_ADAPTER == null) {
            TRACKING_DB_ADAPTER = new TrackingDBAdapter(getDB());
        }
        return TRACKING_DB_ADAPTER;
    }

    /**
     * @param context Kontext
     * @return statischer DB Adapter
     */
    public static TrackingDBAdapter getTrackingDBAdapter(Context context) {
        if (TRACKING_DB_ADAPTER == null) {
            TRACKING_DB_ADAPTER = new TrackingDBAdapter(getDB(context));
        }
        return TRACKING_DB_ADAPTER;
    }

    /**
     * Statische Methode zum Speichern von Zeiterfassungs-Einträgen in der Datenbank
     *
     * @param db       Datenbank, in die die Zeiterfassung eingetragen wird
     * @param trackingEntry Zeitnahme, die gespeichert wird
     * @return ID des gespeicherten Eintrags oder -1 bei Fehlschlag
     */
    public static long insert(SQLiteDatabase db, TrackingEntry trackingEntry) {
        return TableAdapter.insert(db, P.tableName, trackingEntry, staticContentValues(trackingEntry));
    }

    /**
     * Liest einen Zeitnahme-Eintrag anhand einer ID aus der Datenbank.
     *
     * @param db Datenbank, die zum Lesen genutzt wird
     * @param id ID des gesuchten Datenbankeintrags
     * @return TrackingEntry-Objekt oder <tt>null</tt>, wenn unter der ID kein Eintrag vorhanden ist
     */
    @Nullable
    public static TrackingEntry byID(SQLiteDatabase db, long id) {
        final Cursor c = db.query(P.tableName, P.table.getColumnNames(), "_id=" + id, null, null, null, null, "1");
        if (!c.moveToFirst()) {
            Log.DB_LOGGER.w("Tracking-Eintrag mit ID " + id + " nicht in der Datenbank vorhanden!");
            return null;
        }
        final TrackingEntry entry = fromCursor(c);
        c.close();
        return entry;
    }

    /**
     * Erstellt eine TodoListe anhand einer Datenbank-Zeile
     *
     * @param c     Cursor auf die auszulesende Datenbankzeile
     * @return TrackingEntry-Objekt mit den aus der Datenbank gelesenen Werten
     */
    @NonNull
    private static TrackingEntry fromCursor(@NonNull Cursor c) {
        return new TrackingEntry(
                c.getLong(ID.ordinal()),
                dbAsCalendar(c.getLong(BEGIN.ordinal())),
                c.isNull(END.ordinal()) ? null : dbAsCalendar(c.getLong(END.ordinal())),
                c.getString(PROJECT.ordinal()),
                c.getString(DESCRIPTION.ordinal())
        );
    }

    /**
     * Aktualisiert eine TodoListe in der Datenbank
     *
     * @param db       Datenbankverbindung
     * @param trackingEntry Zu aktualisierende TodoListe
     */
    @SuppressWarnings("ConstantConditions")
    public static void update(SQLiteDatabase db, TrackingEntry trackingEntry) {
        TableAdapter.update(db, P.tableName, trackingEntry.getID(), staticContentValues(trackingEntry));
    }

    /**
     * Bildet die Informationen einer Zeiterfassung auf eine Datenbank-kompatible Datenstruktur ab
     *
     * @param entry Abzubildende TodoListe
     * @return Schlüssel-Wert Paare, die in die Datenbank geschrieben werden können
     */
    @NonNull
    private static ContentValues staticContentValues(@NonNull TrackingEntry entry) {
        final ContentValues values = new ContentValues(COLUMN.values().length - 1);
        values.put(P.name(BEGIN), dbAsLong(entry.getBegin()));
        values.put(P.name(END), dbAsLong(entry.getEnd()));
        values.put(P.name(PROJECT), entry.getProject());
        values.put(P.name(DESCRIPTION), entry.getDescription());
        return values;
    }

    /**
     * Liefert eine Liste der Zeittracking-Einträge, die mit einem Projekt verknüpft sind.
     * Wird kein Projekt angegeben, werden alle Zeiteinträge zurückgegeben.
     * Die Einträge werden dabei nach Startzeitpunkt sortiert zurückgegeben.
     *
     * @param project optional gesuchtes Projekt
     * @param limit Begrenzung der zurückgegebenen Einträge auf eine Anzahl, 0 für alle
     * @param offset Offset bei begrenzter Abfrage
     * @return Nach Startzeitpunkt sortierte Liste mit allen Tracking-Einträgen,
     * die zum Projekt gehören.
     */
    @NonNull
    public final List<TrackingEntry> getAllByProject(@Nullable final String project, final int limit, final int
            offset) {
        if (limit < 0 || offset < 0) throw new IllegalArgumentException("Negatives Limit oder Offset");
        final List<TrackingEntry> entries = new ArrayList<>();
        final SQLiteDatabase database = this.db.getReadableDatabase();
        final Cursor c = database.query(P.tableName, P.table.getColumnNames(),
                (project != null) ?
                        P.name(END) + " IS NOT NULL AND project='" + project + "'" :
                        P.name(END) + " IS NOT NULL",
                null, null, null, P.name(BEGIN) + " DESC",
                (limit == 0) ? null : (offset + ", " + limit));
        while (c.moveToNext()) entries.add(fromCursor(c));
        c.close();
        database.close();
        return Collections.unmodifiableList(entries);
    }

    /**
     * Liefert eine Liste der Zeittracking-Einträge, die mit einem Projekt verknüpft sind.
     * Wird kein Projekt angegeben, werden alle Zeiteinträge zurückgegeben.
     * Die Einträge werden dabei nach Startzeitpunkt sortiert zurückgegeben.
     *
     * @param project optional gesuchtes Projekt
     * @return Nach Startzeitpunkt sortierte Liste mit allen Tracking-Einträgen,
     * die zum Projekt gehören.
     */
    @NonNull
    public final List<TrackingEntry> getAllByProject(@Nullable final String project) {
        return getAllByProject(project, 0, 0);
    }

    /**
     * Liefert eine Liste der Zeittracking-Einträge, die mit einem Projekt verknüpft sind.
     * Wird kein Projekt angegeben, werden alle Zeiteinträge zurückgegeben.
     * Die Einträge werden dabei nach Startzeitpunkt sortiert zurückgegeben.
     *
     * @return Nach Startzeitpunkt sortierte Liste mit allen Tracking-Einträgen,
     * die zum Projekt gehören.
     */
    @NonNull
    public final List<TrackingEntry> getAll() {
        return getAllByProject(null, 0, 0);
    }

    /**
     * @return Liefert eine alphabetisch sortierte Menge der bisher verwendeten Projektnamen
     */
    @NonNull
    public final SortedSet<String> getAllProjectNames() {
        SortedSet<String> names = new TreeSet<>();
        final SQLiteDatabase database = this.db.getReadableDatabase();
        final Cursor c = database.query(true, P.tableName, new String[]{P.name(PROJECT)},
                P.name(PROJECT) + " IS NOT NULL", null, null, null, null, null);
        while (c.moveToNext()) {
            final String project = c.getString(0);
            if (!c.isNull(0) && project != null && !project.trim().isEmpty())
                names.add(c.getString(0));
        }
        c.close();
        database.close();
        return names;
    }

    /**
     * Liefert eine alphabetisch sortierte Menge der bisher verwendeten Beschreibungen in einem Projekt
     * @param project Projektname, nach dem gefiltert wird
     * @return alphabetisch sortierte Menge der bisher verwendeten Beschreibungen in einem Projekt
     */
    @NonNull
    public final SortedSet<String> getAllDescriptions(@Nullable String project) {
        SortedSet<String> descriptions = new TreeSet<>();
        final SQLiteDatabase database = this.db.getReadableDatabase();
        final Cursor c = database.query(true, P.tableName, new String[]{P.name(DESCRIPTION)},
                (project == null) ?
                        null : P.name(PROJECT) + "='" + project + "'",
                null, null, null, null, null);
        while (c.moveToNext()) {
            final String description = c.getString(0);
            if (!c.isNull(0) && description != null && !description.trim().isEmpty())
                descriptions.add(c.getString(0));
        }
        c.close();
        database.close();
        return descriptions;
    }

    /**
     * @return Liefert eine alphabetisch sortierte Menge der bisher verwendeten Beschreibungen in allen Projekten
     */
    @NonNull
    public final SortedSet<String> getAllDescriptions() {
        return getAllDescriptions(null);
    }

    /**
     * @return Liefert den laufenden Tracking-Eintrag aus der Datenbank falls vorhanden.
     */
    @Nullable
    public final TrackingEntry getRunningEntry() {
        final SQLiteDatabase database = this.db.getReadableDatabase();
        final Cursor c = database.query(P.tableName, P.table.getColumnNames(), P.name(END) +
                " IS NULL", null, null, null, null, "1");
        final TrackingEntry entry = (c.moveToFirst()) ? fromCursor(c) : null;
        c.close();
        database.close();
        return entry;
    }

    @Override
    public TrackingEntry byID(long id) {
        final SQLiteDatabase database = this.db.getReadableDatabase();
        final TrackingEntry result = byID(database, id);
        database.close();
        return result;
    }

    /**
     * Berechnet die aufgewandte Zeit für ein bestimmtes Projekt
     *
     * @param project Projektfilter, dessen Einträge summiert werden.
     * @return Aufgewendete Zeit in ms
     */
    public long getTrackedTime(@Nullable String project) {
        final SQLiteDatabase database = this.db.getReadableDatabase();
        final Cursor c = database.query(P.tableName,
                new String[]{String.format("SUM(%s - %s)", P.name(END), P.name(BEGIN))},
                (project == null) ?
                        P.name(END) + " IS NOT NULL" :
                        P.name(END) + " IS NOT NULL AND " + P.name(PROJECT) + "='" + project + "'",
                null, null, null, null);
        c.moveToFirst();
        final long time = c.getLong(0);
        c.close();
        database.close();
        return time;
    }

    @NonNull
    @Override
    ContentValues getContentValues(@NonNull TrackingEntry entry) {
        return staticContentValues(entry);
    }

    /**
     * @return Anzahl aller getrackten Einträge
     */
    public final int getTotalEntryCount() {
        return getTotalEntryCount(null);
    }

    /**
     * @param project Einschränkung des Projekt
     * @return Anzahl aller getrackten Einträge
     */
    public final int getTotalEntryCount(@Nullable String project) {
        final SQLiteDatabase database = this.db.getReadableDatabase();
        final Cursor c = database.query(P.tableName, new String[]{"COUNT(" + P.name(ID) + ")"},
                (project == null) ?
                        P.name(END) + " IS NOT NULL" :
                        String.format("%s IS NOT NULL AND %s='%s'", P.name(END), P.name(PROJECT), project),
                null, null, null, null);
        c.moveToFirst();
        final int count = c.getInt(0);
        c.close();
        database.close();
        return count;
    }

    /**
     * Berechnet die aufgewandte Zeit für alle Projekte
     *
     * @return Aufgewendete Zeit in ms
     */
    public long getTrackedTime() {
        return getTrackedTime(null);
    }

    /** Aufzählung der Spalten */
    public enum COLUMN {
        ID, // obligatorische ID-Spalte
        BEGIN, // Startzeitpunkt
        END, // Endzeitpunkt
        PROJECT, // Projektname
        DESCRIPTION, // Beschreibung
    }
}
