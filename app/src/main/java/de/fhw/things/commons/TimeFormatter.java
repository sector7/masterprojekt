package de.fhw.things.commons;

import android.support.annotation.NonNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Hilfsklasse zur Formatierung von Zeitangaben.
 */
public class TimeFormatter {
    /**
     * Formatiert die Dauer des Eintrags als Zeitdauer auf Basis des gegebenen Zeitformats.
     *
     * @param format Format nach
     *               <a href="http://www.unicode.org/reports/tr35/tr35-dates.html#Date_Format_Patterns">UTS #35</a>
     * @param duration Dauer in ms
     * @return formatierte Zeit
     */
    @NonNull
    private static String formatDuration(@NonNull String format, long duration) {
        DateFormat df = new SimpleDateFormat(format, Locale.ROOT);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format(new Date(duration));
    }

    /**
     * Formatiert eine Zeitangabe.
     * @param duration Zeit in ms
     * @param stripSeconds Angabe ob Sekunden bei Zeitspannen von mindestens einem Tag weggelassen werden sollen
     * @return formatierter String
     */
    public static String formatTime(long duration, boolean stripSeconds){
        int days = getDays(duration);
        if(days > 0) {
            if(stripSeconds){
                return days + "d " + formatDuration("H'h 'm'm'", duration);
            } else {
                return days + "d " + formatDuration("H'h 'm'm 's's'", duration);
            }
        } else {
            return formatDuration("HH:mm:ss", duration);
        }
    }

    /**
     * Liefert die Anzahl an Tagen, die das Tracking Entry läuft.
     * @param duration Zeit in ms
     * @return Anzahl an Tagen
     */
    private static int getDays(long duration){
        return (int) (duration / (1000*60*60*24));
    }

}
