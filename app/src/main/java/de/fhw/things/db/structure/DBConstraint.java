package de.fhw.things.db.structure;

/**
 * Restriktionen für Datenbankspalten.
 */
public abstract class DBConstraint {

    /** Standardrestriktion für ID-Spalte. */
    public static final DBConstraint ID = new PrimaryKeyConstraint(true);

    /** Singleton für erforderliche Werte. */
    public static final DBConstraint NOT_NULL = new DBConstraint() {
        @Override
        public String getSQLiteDefinition() {
            return "NOT NULL";
        }
    };

    /** @return SQLite-Statement der Restriktion */
    public abstract String getSQLiteDefinition();

    /** Restriktion für Schlüsselwerte. */
    private static class PrimaryKeyConstraint extends DBConstraint {

        /** Auto-Inkrement für Schlüssel aktiviert? */
        private final boolean autoIncrement;

        /** Optionale Ordnung der Schlüssel */
        private final Order order;

        /**
         * Konstruktor für Primary Keys
         *
         * @param autoIncrement Auto-Inkrement aktiviert?
         */
        @SuppressWarnings("SameParameterValue")
        private PrimaryKeyConstraint(boolean autoIncrement) {
            this(autoIncrement, null);
        }

        /**
         * Konstruktor für Primary Keys
         *
         * @param autoIncrement Auto-Inkrement aktiviert?
         * @param order         Aufsteigende bzw. absteigende Reihenfolge für Keygenerierung
         */
        @SuppressWarnings("SameParameterValue")
        private PrimaryKeyConstraint(boolean autoIncrement, Order order) {
            this.autoIncrement = autoIncrement;
            this.order = order;
        }

        @Override
        public final String getSQLiteDefinition() {
            return "PRIMARY KEY" +
                    (this.autoIncrement ? (this.order == null ? "" : this.order.toString()) + " AUTOINCREMENT" : "");
        }

        /**
         * Auf- oder absteigendes Auto-Inkrement
         */
        public enum Order {
            ASC,
            DESC
        }
    }
}
