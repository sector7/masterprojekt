package de.fhw.things.db.structure;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import com.google.common.collect.ImmutableMap;

import java.util.Arrays;
import java.util.Map;

import de.fhw.things.logger.Log;

public class DBTableTest extends AndroidTestCase {

    private SQLiteDatabase db;
    private DBTable testTable = new DBTable(new TableProperties() {
        @Override
        protected Map fillColumnMap() {
            return ImmutableMap.of("Column1", new DBColumn("Column1", DBColumn.REAL));
        }

        @Override
        public String getTableName() {
            return "testTable";
        }
    });

    private DBTable anotherTestTable = new DBTable(new TableProperties() {
        @Override
        protected Map fillColumnMap() {
            return ImmutableMap.of(
                    "_id", DBColumn.COLUMN_ID,
                    "fkey", new DBForeignKey(DBTableTest.this.testTable),
                    "nonNullBoolean", new DBColumn("nonNullBoolean", DBColumn.BOOLEAN, DBConstraint.NOT_NULL));
        }

        @Override
        public String getTableName() {
            return "anotherTestTable";
        }
    });

    @Override
    public void setUp() throws Exception {
        super.setUp();
        RenamingDelegatingContext context = new RenamingDelegatingContext(getContext(), "test_");
        for(String dbName : context.databaseList()){
            context.deleteDatabase(dbName);
        }
        this.db = DB.getDB(context).getWritableDatabase();
    }

    @Override
    public void tearDown() throws Exception {
        this.db.close();
        super.tearDown();
    }

    public void testCreate() throws Exception {
        this.testTable.create(this.db);
        Cursor c = this.db.rawQuery("SELECT sql FROM sqlite_master WHERE type='table' AND name='testTable'", null);
        Log.TEST_LOGGER.d(Arrays.toString(c.getColumnNames()));
        assertTrue(c.moveToFirst());
        assertEquals(c.getString(0), "CREATE TABLE " + this.testTable.getSQLiteDefinition());
        c.close();
    }

    public void testGetSQLiteDefinition() throws Exception {
        assertEquals("testTable (Column1 REAL)", this.testTable.getSQLiteDefinition());
        assertEquals("anotherTestTable (_id INTEGER PRIMARY KEY AUTOINCREMENT, testTable_id INTEGER REFERENCES " +
                "testTable, nonNullBoolean INTEGER NOT NULL)", this.anotherTestTable.getSQLiteDefinition());
    }
}