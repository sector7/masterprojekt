package de.fhw.things.commons;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.SortedSet;

import de.fhw.things.R;
import de.fhw.things.todo.TodoActivity;
import de.fhw.things.tracking.TrackingActivity;
import de.fhw.things.tracking.data.TrackingEntry;
import de.fhw.things.trackinghistory.TrackingHistoryActivity;

import static de.fhw.things.commons.TimeFormatter.formatTime;

/**
 * Kapselt nützliche Methoden rund um das User Interface, die wiederkehrend benötigt werden.
 */
public class UIFunctions extends AppCompatActivity{
    /**
     * Versteckt das Softkeyboard und löst den Fokus von etwaigen EditTexts.
     * @param context Context
     * @param view View auf der der Focus weggenommen werden soll
     */
    public static void hideSoftkeyboardAndRemoveFocus(Context context, View view) {
        if (view != null) {
            view.clearFocus();
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Erzeugt einen Handler zur Anzeige eines laufenden Timers.
     * @param trackingEntry Zeiteintrag dessen Zeit dargestellt werden soll
     * @param textView Feld in dem die Zeit angezeigt werden soll
     * @param defaultStr Default-String wenn Eintrag null
     * @return handler
     */
    public static DisplayTimerHandler displayTimer(@Nullable TrackingEntry trackingEntry, TextView textView, String
            defaultStr) {
        return new DisplayTimerHandler(trackingEntry, textView, defaultStr);
    }

    /**
     * Aktualisiert die Vorschlagslisten für einen Projektnamen aller Projektfelder.
     */
    public static void updateSuggestions(Context context, SortedSet<String> projectNames, List<AutoCompleteTextView> views) {
        Object[] objects = projectNames.toArray();
        String[] stringArray = Arrays.copyOf(objects, objects.length, String[].class);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                android.R.layout.simple_dropdown_item_1line, stringArray);

        for(AutoCompleteTextView view: views)
            view.setAdapter(adapter);
    }

    /**
     * Setzt die aktivierten Buttons zum switchen der activity.
     * @param activity Activity
     * @param activityClass Klasse der angezeigten Activity
     */
    public static void switchActivatedActivityButtons(Activity activity, Class activityClass) {
        activity.findViewById(R.id.buttonStartTodoActivity).setActivated(
                TodoActivity.class.equals(activityClass)
        );
        activity.findViewById(R.id.buttonStartTrackingActivity).setActivated(
                TrackingActivity.class.equals(activityClass)
        );
        activity.findViewById(R.id.buttonStartTrackingHistoryActivity).setActivated(
                TrackingHistoryActivity.class.equals(activityClass)
        );
    }

    /**
     * Ruft eine gewünschte Activity auf, wenn man sich noch nicht dort befindet.
     * @param activity Aufrufende Activity
     * @param caller Klasse der aufrufenden Activity
     * @param dest Activity, die geöffnet werden soll
     */
    public void switchActivity(Activity activity, Class caller, Class dest) {
        if (!caller.equals(dest)) {
            Intent intent = new Intent(activity, dest);
            activity.startActivity(intent);
            switchActivatedActivityButtons(activity, dest);
            activity.finish();
        }
    }

    /**
     * Ruft eine gewünschte Activity auf.
     * @param activity Aufrufende Activity
     * @param dest Activity, die geöffnet werden soll
     * @param intent Intent
     */
    public void switchActivity(Activity activity, Class dest, Intent intent) {
        activity.startActivity(intent);
        switchActivatedActivityButtons(activity, dest);
        activity.finish();
    }

    /**
     * Handler zur Anzeige eines Timers.
     */
    public static class DisplayTimerHandler extends Handler {

        /** Zeiteintrag dessen Dauer angezeigt werden soll */
        @Nullable
        TrackingEntry trackingEntry;
        /** Feld in dem die Zeit angezeigt werden soll */
        @NonNull
        final TextView textView;
        /** Default-String wenn Eintrag null */
        @NonNull
        final String defaultStr;

        /** Runnable das sich um das Triggern der Anzeige kümmert */
        private final Runnable DISPLAY_RUNNING_TIMER = new Runnable() {
                @Override
                public void run() {
                    displayTime();
                    // Handler nochmal aufrufen
                    DisplayTimerHandler.this.restart();
                }
            };

        /**
         * Erstellt und startet den Timer zur Anzeige der verstrichenen Zeit im tracking Entry.
         * @param trackingEntry Entry dessen Zeit angezeigt werden soll
         * @param textView Feld in dem die Zeit angezeigt werden soll
         * @param defaultStr String, der angezeigt werden soll, wenn das tracking entry null ist
         */
        public DisplayTimerHandler(@Nullable TrackingEntry trackingEntry, @NonNull TextView textView, @NonNull String
                defaultStr) {
            this.trackingEntry = trackingEntry;
            this.textView = textView;
            this.defaultStr = defaultStr;
            if(this.trackingEntry != null){
                this.restart();
            }
        }

        /**
         * Triggert die wiederholte Anzeige des laufenden Timers.
         * @param trackingEntry Zeiteintrag der dargestellt werden soll
         */
        public void start(TrackingEntry trackingEntry) {
            this.trackingEntry = trackingEntry;
            this.postDelayed(this.DISPLAY_RUNNING_TIMER, 250);
        }

        /**
         * Triggert die wiederholte Anzeige des laufenden Timers mit dem aktuell gesetzten Zeiteintrag.
         */
        public void restart() {
            this.postDelayed(this.DISPLAY_RUNNING_TIMER, 250);
        }

        /**
         * Stoppt die wiederholte Anzeige des laufenden Timers.
         */
        public void stop() {
            this.removeCallbacks(this.DISPLAY_RUNNING_TIMER);
        }

        /**
         * Ermittelt die String-Repräsenation der vergangenen Zeit und setzt diese in dem Text Feld.
         */
        private void displayTime(){
            this.textView.setText(
                    (this.trackingEntry != null)
                            ? formatTime(this.trackingEntry.getDuration(), false)
                            : this.defaultStr
            );
        }
    }

}
