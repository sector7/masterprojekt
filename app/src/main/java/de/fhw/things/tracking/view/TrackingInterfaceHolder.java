package de.fhw.things.tracking.view;

import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import de.fhw.things.R;
import de.fhw.things.tracking.TrackingActivity;
import de.fhw.things.tracking.data.TrackingEntry;

/**
 * Hält die Elemente des Tracking Interfaces bereit und setzt die Layoutspezifischen Callbacks.
 */
public class TrackingInterfaceHolder {
    /** Interface zur Angabe des Projektnamens */
    public final TrackingInputHolder project;
    /** Interface zur Angabe des Aufgaben-Namens */
    public final TrackingInputHolder description;
    /** Anzeige der aktuellen Zeit */
    public final TextView time;
    /** Button zum starten des Timers */
    public final ImageButton startButton;
    /** Button zum beenden des Timers */
    public final ImageButton stopButton;
    /** Placeholder für das Feld zur Angabe des Projektes */
    public static final String HINT_PROJECT = "Projekt";
    /** Placeholder für das Feld zur Angabe des Tasks */
    public static final String HINT_TASK = "Beschreibung";

    /**
     * Liest die Elemente aus und setzt die Layoutspezifischen Callbacks
     * @param trackingActivity Tracking Acitvity
     */
    public TrackingInterfaceHolder(TrackingActivity trackingActivity){
        this.time = (TextView) trackingActivity.findViewById(R.id.trackingInterface_time);
        this.startButton = (ImageButton) trackingActivity.findViewById(R.id.trackingInterface_button_startTimer);
        this.stopButton = (ImageButton) trackingActivity.findViewById(R.id.trackingInterface_button_stopTimer);
        this.project = new TrackingInputHolder(trackingActivity.findViewById(R.id.trackingInterface_project), HINT_PROJECT);
        this.description = new TrackingInputHolder(trackingActivity.findViewById(R.id.trackingInterface_task), HINT_TASK);
    }

    /**
     * @param entry Eintrag, dessen Informationen angezeigt werden sollen
     */
    public void initInterface(TrackingEntry entry){
        this.project.description.setText(entry.getProject());
        this.description.description.setText(entry.getDescription());
        if(entry.isRunning()){
            switchButtons();
        }
    }

    /**
     * Tauscht die Anzeige der Buttons aus.
     */
    public void switchButtons(){
        switchButtonVisibility(this.startButton);
        switchButtonVisibility(this.stopButton);
    }

    /**
     * Toggled die Anzeige eines Buttons.
     * @param button Button
     */
    public void switchButtonVisibility(ImageButton button){
        button.setVisibility(
                button.getVisibility()== View.VISIBLE ? View.GONE : View.VISIBLE
        );
    }

    /**
     * Holder für das Layout des gruppieren Input feldes mit zugehörigem Button im Tracking Interface.
     */
    public class TrackingInputHolder {
        /** Beschreibung */
        public final AutoCompleteTextView description;
        /** Button zum löschen des Inhalts */
        public final Button clear;

        /**
         * Liest die Elemente aus.
         * @param view Parent View
         */
        public TrackingInputHolder(View view, String hint) {
            this.description = (AutoCompleteTextView) view.findViewById(R.id.trackingInput_name);
            this.description.setThreshold(0);
            this.description.setHint(hint);
            this.clear = (Button) view.findViewById(R.id.trackingInput_button);

            // on-click zum leeren des Feldes
            // wird hier im Holder gemacht, weil das nur eine reine Frontend Interaktion ist
            // und keine Adapter oder Ähnliches getriggert werden
            this.clear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    description.setText("");
                    description.requestFocus();
                }
            });
        }

        /**
         * @return Eingetragener Text
         */
        public String getText(){
            return this.description.getText().toString();
        }
    }

}