package de.fhw.things.trackinghistory.view.listener;

import android.app.DatePickerDialog;
import android.view.View;

import java.util.Calendar;

import de.fhw.things.tracking.data.TrackingEntry;
import de.fhw.things.trackinghistory.TrackingHistoryActivity;

/**
 * Listener für den Datums-Auswahl Dialog.
 */
public class DatePickerListener implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    /** Zugehörige Activity */
    private final TrackingHistoryActivity parent;
    /** zugehöriger Tracking-Eintrag */
    private final TrackingEntry entry;
    /** Frontend-Tag des Eintrags */
    private final int tag;
    /** Flag, ob Anfangs-Zeitpunkt (sonst Endzeitpunkt) */
    private final boolean isBegin;

    /**
     * Erzeugt einen Listener auf einem Textfeld, das ein Datum repräsentiert.
     * @param entry Kalenderobjekt des Datums
     */
    public DatePickerListener(TrackingHistoryActivity parent, TrackingEntry entry, int tag, boolean isBegin) {
        this.parent = parent;
        this.entry = entry;
        this.tag = tag;
        this.isBegin = isBegin;
    }

    @Override
    public void onDateSet(android.widget.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar current;
        if(isBegin)
            current = this.entry.getBegin();
        else
            current = this.entry.getEnd();

        // Nur etwas tun, wenn das Datum sich verändert hat
        if(current != null && (current.get(Calendar.YEAR) != year
                || current.get(Calendar.MONTH) != monthOfYear
                || current.get(Calendar.DAY_OF_MONTH) != dayOfMonth)) {

            Calendar newDate = Calendar.getInstance();
            newDate.setTimeInMillis(current.getTimeInMillis());
            newDate.set(year, monthOfYear, dayOfMonth);

            parent.setDate(newDate, this.tag, this.isBegin);
        }
    }

    @Override
    public void onClick(View v) {
        Calendar current;
        if(isBegin)
            current = this.entry.getBegin();
        else
            current = this.entry.getEnd();

        if(current != null) {
            DatePickerDialog dialog = new DatePickerDialog(
                    v.getContext(),
                    this,
                    current.get(Calendar.YEAR),
                    current.get(Calendar.MONTH),
                    current.get(Calendar.DAY_OF_MONTH));
            dialog.show();
        }
    }
}