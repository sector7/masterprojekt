package de.fhw.things.test.todo;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

import de.fhw.things.todo.data.beans.TodoEntry;
import de.fhw.things.todo.data.beans.TodoList;

/**
 * Testet die Funktionalität der Todoliste.
 */
public class TodoListTest extends TestCase {

    private TodoList todoList;

    public void setUp() {
        this.todoList = new TodoList("Test");
        List<TodoEntry> entries = new ArrayList<>();
        for (int i = 0; i < 10; i++)
            entries.add(new TodoEntry(this.todoList, "Aufgabe " + (char) ('A' + i), i)); // lol

        this.todoList.setEntries(entries);
    }
    
    public void testMoveEntryToPosition() throws Exception {
        this.todoList.moveEntryToPosition(5, 2);

        assertPositions();
        assertEquals("Aufgabe A", this.todoList.getEntries().get(0).getText());
        assertEquals("Aufgabe B", this.todoList.getEntries().get(1).getText());
        assertEquals("Aufgabe F", this.todoList.getEntries().get(2).getText());
        assertEquals("Aufgabe C", this.todoList.getEntries().get(3).getText());
        assertEquals("Aufgabe D", this.todoList.getEntries().get(4).getText());
        assertEquals("Aufgabe E", this.todoList.getEntries().get(5).getText());
        assertEquals("Aufgabe G", this.todoList.getEntries().get(6).getText());
        assertEquals("Aufgabe H", this.todoList.getEntries().get(7).getText());
        assertEquals("Aufgabe I", this.todoList.getEntries().get(8).getText());
        assertEquals("Aufgabe J", this.todoList.getEntries().get(9).getText());

        this.todoList.moveEntryToPosition(2, 5);
        assertPositions();
        for (int i = 0; i < 10; i++)
            assertEquals("Aufgabe " + (char) ('A' + i), this.todoList.getEntries().get(i).getText());
    }

    public void testMoveEntryToFirst() {
        this.todoList.moveEntryToPosition(5, 0);
        assertPositions();
        assertEquals("Aufgabe F", this.todoList.getEntries().get(0).getText());
        assertEquals("Aufgabe A", this.todoList.getEntries().get(1).getText());
        assertEquals("Aufgabe B", this.todoList.getEntries().get(2).getText());
    }

    public void testMoveEntryToLast() {
        this.todoList.moveEntryToPosition(5, 9);
        assertPositions();
        assertEquals("Aufgabe I", this.todoList.getEntries().get(7).getText());
        assertEquals("Aufgabe J", this.todoList.getEntries().get(8).getText());
        assertEquals("Aufgabe F", this.todoList.getEntries().get(9).getText());
    }

    private void assertPositions() {
        for (int i = 0; i < this.todoList.getEntries().size(); i++) {
            TodoEntry entry = this.todoList.getEntries().get(i);
            assertEquals(i, entry.getPosition().intValue());
        }
    }
}