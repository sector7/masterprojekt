package de.fhw.things.tracking;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

import java.util.Collections;

import de.fhw.things.R;
import de.fhw.things.commons.UIFunctions;
import de.fhw.things.commons.UIFunctions.DisplayTimerHandler;
import de.fhw.things.db.adapter.TrackingDBAdapter;
import de.fhw.things.todo.TodoActivity;
import de.fhw.things.tracking.data.TrackingEntry;
import de.fhw.things.tracking.view.TrackingInterfaceHolder;
import de.fhw.things.trackinghistory.TrackingHistoryActivity;

import static de.fhw.things.commons.UIFunctions.displayTimer;
import static de.fhw.things.commons.UIFunctions.hideSoftkeyboardAndRemoveFocus;
import static de.fhw.things.commons.UIFunctions.switchActivatedActivityButtons;
import static de.fhw.things.commons.UIFunctions.updateSuggestions;
import static de.fhw.things.db.adapter.TrackingDBAdapter.getTrackingDBAdapter;

/**
 * Acitivity zur Darstellung des Tracking-Interfaces.
 */
public class TrackingActivity extends AppCompatActivity {
    /** Holder für den Zugriff auf die Frontend-Komponenten */
    private TrackingInterfaceHolder trackingInterfaceHolder;
    /** Liefert den DB Adapter */
    private final TrackingDBAdapter trackingDBAdapter = TrackingDBAdapter.getTrackingDBAdapter(this);
    /** Laufender Timer oder null */
    private TrackingEntry trackingEntry;
    /** Default Zeit für die Anzeige */
    private String START_TIME;
    /** Handler zum refresh der Timer-Anzeige */
    private DisplayTimerHandler timerRefreshHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUI();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        initializeUI();
    }

    /**
     * Initialisiert das User Interface.
     */
    private void initializeUI() {
        setContentView(R.layout.activity_tracking);
        this.trackingInterfaceHolder = new TrackingInterfaceHolder(this);
        this.START_TIME = getString(R.string.starttime);
        setCallbacks();

        // Zeiteintrag initialisieren
        loadRunningEntry();
        this.timerRefreshHandler = displayTimer(
                this.trackingEntry, this.trackingInterfaceHolder.time, this.START_TIME);
        onNewTrackingIntent();

        // Liste der aktuellen Projekte und Beschreibungen zur Autovervollständigung laden
        updateSuggestions(this,
                getTrackingDBAdapter(this).getAllProjectNames(),
                Collections.singletonList(this.trackingInterfaceHolder.project.description));
        updateSuggestions(this,
                getTrackingDBAdapter(this).getAllDescriptions(),
                Collections.singletonList(this.trackingInterfaceHolder.description.description));

        // Setzen eines Listeners zum Beenden des Bearbeiten-Modus beim Druck auf Fertig.
        setClearFocusOnHideKeyboard();

        switchActivatedActivityButtons(this, this.getClass());
    }

    /**
     * Entfernt den Fokus aus den Textfeldern beim schließen der Tastatur.
     */
    private void setClearFocusOnHideKeyboard() {
        this.trackingInterfaceHolder.project.description.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                    hideSoftkeyboardAndRemoveFocus(TrackingActivity.this, TrackingActivity.this.trackingInterfaceHolder.project.description);
                return false;
            }
        });
        this.trackingInterfaceHolder.description.description.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                    hideSoftkeyboardAndRemoveFocus(TrackingActivity.this, TrackingActivity.this.trackingInterfaceHolder.description.description);
                return false;
            }
        });
    }

    /**
     * Lädt einen noch laufenden Eintrag und bringt ihn zur Anzeige.
     */
    private void loadRunningEntry() {
        this.trackingEntry = this.trackingDBAdapter.getRunningEntry();
        if (this.trackingEntry != null) {
            this.trackingInterfaceHolder.initInterface(this.trackingEntry);
        }
    }

    /**
     * Startet ein Tracking wenn die gewünschten Parameter gesetzt sind.
     */
    private void onNewTrackingIntent(){
        Intent intent = getIntent();
        String project = intent.getStringExtra("project");
        String description = intent.getStringExtra("description");
        if((project != null) && (description != null)){
            // Alten Timer stoppen
            if(this.trackingEntry != null && this.trackingEntry.isRunning()){
                stopTimer(null);
            }
            // Neuen Timer starten
            this.trackingInterfaceHolder.project.description.setText(project);
            this.trackingInterfaceHolder.description.description.setText(description);
            startTimer(null);
        }
    }

    /**
     * Setzt die benötigten Callbacks
     */
    private void setCallbacks(){
        // Neue Projektname speichern, bei jeder Änderung, da nicht garantiert ist,
        // dass der Benutzer das EditText tatsächlich verlässt (kein onFocusChanged)
        this.trackingInterfaceHolder.project.description.addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        String str = s.toString();
                        updateProjectIfTimerIsRunning(str);
                        toggleClearButton(str, TrackingActivity.this.trackingInterfaceHolder.project.clear);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                }
        );
        // Neue Beschreibung speichern, bei jeder Änderung, da nicht garantiert ist,
        // dass der Benutzer das EditText tatsächlich verlässt (kein onFocusChanged)
        this.trackingInterfaceHolder.description.description.addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        String str = s.toString();
                        updateDescriptionIfTimerIsRunning(str);
                        toggleClearButton(str, TrackingActivity.this.trackingInterfaceHolder.description.clear);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                }
        );
    }

    /**
     * Togglet die Anzeige des Clear-Button, je nachdem ob etwas in dem zugehörigen Feld eingetragen ist.
     * @param str eingetragener String
     * @param button Button zum leeren des Feldes
     */
    private void toggleClearButton(String str, Button button){
        if(str.length()>0){
            button.setTextColor(ContextCompat.getColor(this, R.color.grey));
        } else {
            button.setTextColor(ContextCompat.getColor(this, R.color.grey_lightest));
        }
    }

    /**
     * Aktualisiert den Projektnamen und speichert die Änderung in der Datenbank.
     * @param str eingetragener Name
     */
    private void updateProjectIfTimerIsRunning(String str){
        // Nur wenn bereits ein Timer läuft, also ein Tracking Entry gesetzt ist
        if(this.trackingEntry != null){
            this.trackingEntry.setProject(str);
            this.trackingDBAdapter.upsert(this.trackingEntry);
        }
    }

    /**
     * Aktualisiert die Beschreibung und speichert die Änderung in der Datenbank.
     * @param str eingetragene Beschreibung
     */
    private void updateDescriptionIfTimerIsRunning(String str){
        // Nur wenn bereits ein Timer läuft, also ein Tracking Entry gesetzt ist
        if(this.trackingEntry != null) {
            this.trackingEntry.setDescription(str);
            this.trackingDBAdapter.upsert(this.trackingEntry);
        }
    }

    /**
     * Startet den Timer und tauscht die Buttons aus.
     * @param view View
     */
    public void startTimer(View view) {
        this.trackingInterfaceHolder.switchButtons();
        // Einzige stelle an der ein Tracking Entry für diese Activity erzeugt wird
        this.trackingEntry = new TrackingEntry(
                this.trackingInterfaceHolder.project.getText(),
                this.trackingInterfaceHolder.description.getText());
        this.timerRefreshHandler.start(this.trackingEntry);
        this.trackingDBAdapter.insert(this.trackingEntry);
    }

    /**
     * Stoppt den Timer, triggert das Speichern und tauscht die Buttons aus.
     * @param view View
     */
    public void stopTimer(View view) {
        this.trackingInterfaceHolder.switchButtons();
        this.trackingEntry.stopTracking();
        this.timerRefreshHandler.stop();
        this.trackingDBAdapter.upsert(this.trackingEntry);
        this.trackingEntry = null;
        clear();
    }

    /**
     * Setzt das Interface zurück.
     */
    private void clear(){
        this.trackingInterfaceHolder.project.description.setText("");
        this.trackingInterfaceHolder.description.description.setText("");
        this.trackingInterfaceHolder.time.setText(this.START_TIME);
    }

    /**
     * Startet die Tracking-Activity.
     * @param view Button
     */
    public void startTrackingActivity (View view) {
        // Hier passiert nichts
    }

    /**
     * Startet die TodoAcitvity.
     * @param view Button
     */
    public void startTodoActiviy(View view) {
        new UIFunctions().switchActivity(this, TrackingActivity.class, TodoActivity.class);
    }

    /**
     * Startet die TrackingHistory-Activity
     * @param view Button
     */
    public void startTrackingHistoryActivity(View view) {
        new UIFunctions().switchActivity(this, TrackingActivity.class, TrackingHistoryActivity.class);
    }
}
