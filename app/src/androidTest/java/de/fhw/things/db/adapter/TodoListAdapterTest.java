package de.fhw.things.db.adapter;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;
import android.util.Pair;

import org.junit.After;
import org.junit.Before;

import java.util.List;

import de.fhw.things.todo.data.beans.TodoEntry;
import de.fhw.things.todo.data.beans.TodoList;

/**
 * Testet den TodoListAdapter.
 */
public class TodoListAdapterTest extends AndroidTestCase {

    /** Testee */
    private TodoListDBAdapter adapter;
    /** Leere Testliste */
    private TodoList emptyList;
    /** Testliste mit Einträgen */
    private TodoList nonEmptyList;
    /** Beispiel-Einträge */
    private TodoEntry[] sampleEntries;
    /** Fake-Kontext */
    private RenamingDelegatingContext context;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.context = new RenamingDelegatingContext(getContext(), "test_");
        this.adapter = TodoListDBAdapter.getTodoListDBAdapter(this.context);
        this.emptyList = new TodoList("TestListe");
        this.sampleEntries = new TodoEntry[5];
        this.nonEmptyList = new TodoList("Liste mit Einträgen");
        for (int i = 0; i < this.sampleEntries.length; i++)
            this.nonEmptyList.addEntry(this.sampleEntries[i] = new TodoEntry("Eintrag " + i));
    }

    @After
    public void tearDown() throws Exception {
        for(Pair<Long, String> todoList : this.adapter.getAllTodoLists()){
            this.adapter.delete(todoList.first);
        }
        super.tearDown();
    }

    public void testUpdate() throws Exception {
        // Given
        long id = this.adapter.insert(this.nonEmptyList);
        TodoList todoList = this.adapter.byID(id);
        todoList.setTitle("neuer Titel");
        // When
        this.adapter.update(todoList);
        // Then
        todoList = this.adapter.byID(id);
        assertEquals("neuer Titel", todoList.getTitle());
    }

    public void testByID() throws Exception {
        // Given / When
        long id = this.adapter.insert(this.emptyList);
        TodoList firstList = this.adapter.byID(id);
        // Then
        assertNotNull(firstList);
        assertEquals(this.emptyList.getTitle(), firstList.getTitle());
        assertTrue(firstList.getEntries().isEmpty());
        assertEquals(0, firstList.getSize());
        assertEquals(id, firstList.getID().longValue());
    }

    public void testInsert_emptyList() throws Exception {
        // Given / When
        long id = this.adapter.insert(this.emptyList);
        // Then
        assertNotNull(this.emptyList.getID());
        assertTrue(this.emptyList.getEntries().isEmpty());
        assertNotNull("Id: " + id, this.adapter.byID(id));
    }

    public void testInsert_nonEmptyList() throws Exception {
        // Given / When
        this.adapter.insert(this.nonEmptyList);
        // Then
        assertNotNull(this.nonEmptyList.getID());
        TodoList fromDB = this.adapter.byID(this.nonEmptyList.getID());
        assertNotNull(fromDB);
        assertTrue(fromDB.getEntries().isEmpty()); // as child entries are not stored on insert()
    }

    public void testGetAllTodoLists() throws Exception {
        // Given
        this.adapter.insert(this.emptyList);
        this.adapter.insert(this.nonEmptyList);
        // When
        List<Pair<Long, String>> todoLists = this.adapter.getAllTodoLists();
        // Then
        assertEquals(2, todoLists.size());
        assertEquals(this.emptyList.getID(), todoLists.get(0).first);
        assertEquals(this.emptyList.getTitle(), todoLists.get(0).second);
        assertEquals(this.nonEmptyList.getID(), todoLists.get(1).first);
        assertEquals(this.nonEmptyList.getTitle(), todoLists.get(1).second);
    }

    public void testResetAllEntries() throws Exception {
        // Given
        for(TodoEntry entry : this.nonEmptyList.getEntries()){
            entry.setDone(true);
        }
        this.adapter.upsert(this.nonEmptyList);
        for(TodoEntry entry : this.adapter.byID(this.nonEmptyList.getID()).getEntries()){
            assertTrue(entry.isDone());
        }
        // When
        this.adapter.resetEntries(this.nonEmptyList);
        // Then
        for(TodoEntry entry : this.adapter.byID(this.nonEmptyList.getID()).getEntries()){
            assertFalse(entry.isDone());
        }
    }
}