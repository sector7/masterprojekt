package de.fhw.things.logger;

import android.util.Log;

/**
 * Klasse zur Kennzeichnung der Ausgaben mit einem einheitlichen Label.
 */
public class Logger {

    /** Label zur einheitlichen Kennzeichnung der Ausgaben */
    private final String label;

    /**
     * Konfiguriert den Logger.
     * @param label Label zur Kennzeichnung der Ausgaben
     */
    Logger(String label){
        this.label = label;
    }

    /**
     * @param msg Fehler-Nachricht
     */
    public void e(String msg){
        Log.e(this.label, msg);
    }

    /**
     * @param msg Debug-Nachricht
     */
    public void d(String msg){
        Log.d(this.label, msg);
    }

    /**
     * @param msg Info
     */
    public void i(String msg){
        Log.i(this.label, msg);
    }

    /**
     * @param msg Warnung
     */
    public void w(String msg){
        Log.w(this.label, msg);
    }

    /**
     * @param msg Plappermaul-Nachricht - darf alles sagen was sie möchte, auch wenn es eigentlich
     *            niemanden interessiert.
     */
    public void v(String msg){
        Log.v(this.label, msg);
    }
}
