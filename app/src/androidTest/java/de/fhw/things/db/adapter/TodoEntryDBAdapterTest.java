package de.fhw.things.db.adapter;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import org.junit.After;
import org.junit.Before;

import java.util.ArrayList;

import de.fhw.things.db.structure.DB;
import de.fhw.things.todo.data.beans.TodoEntry;
import de.fhw.things.todo.data.beans.TodoList;

import static de.fhw.things.db.adapter.TodoEntryDBAdapter.byTodoList;

/**
 * Testklasse zur Abdeckung des Todolist-Adapters.
 */
public class TodoEntryDBAdapterTest extends AndroidTestCase {

    private static final long LIST_ID = 0l;
    /** Testee */
    TodoEntryDBAdapter adapter;
    /** Todoliste */
    TodoList list;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        RenamingDelegatingContext context = new RenamingDelegatingContext(getContext(), "test_");
        this.adapter = TodoEntryDBAdapter.getTodoEntryDbAdapter(context);
        this.list = new TodoList(LIST_ID, "ToDos", new ArrayList<TodoEntry>());
    }

    @After
    public void tearDown() {
        for(TodoEntry entry : byTodoList(DB.getDB().getReadableDatabase(), this.list, null)){
            this.adapter.delete(entry);
        }
    }

    public void testInsert() throws Exception {
        // Given
        TodoEntry entry = new TodoEntry(null, this.list, "Eins", false, false, 0);
        // When
        long id = this.adapter.insert(entry);
        // Then
        assertEquals(Long.valueOf(id), entry.getID());
        shouldBeEqual(this.adapter.byID(id), entry);
    }

    public void testByTodoList() throws Exception {
        // Given
        TodoEntry entry1 = new TodoEntry(null, this.list, "Eins", false, false, 0);
        TodoEntry entry2 = new TodoEntry(null, this.list, "Zwei", false, false, 1);
        // When
        this.adapter.insert(entry1);
        this.adapter.insert(entry2);
        ArrayList<TodoEntry> entries = byTodoList(DB.getDB().getReadableDatabase(), this.list, null);
        // Then
        assertEquals(2, entries.size());
        shouldBeEqual(entry1, entries.get(0));
        shouldBeEqual(entry2, entries.get(1));
    }

    public void testUpsert_shouldUpdate() throws Exception {

    }

    public void testUpsert_shouldInsert() throws Exception {
        // Given
        TodoEntry entry = new TodoEntry(null, this.list, "Eins", false, false, 0);
        this.adapter.upsert(entry);
        entry.setText("Neuer Name");
        entry.setDone(true);
        entry.setPrioritized(true);
        // When
        long id = this.adapter.upsert(entry);
        // Then
        TodoEntry dbEntry = this.adapter.byID(id);
        assertTrue(dbEntry.isDone());
        assertTrue(dbEntry.isPrioritized());
        assertEquals("Neuer Name", dbEntry.getText());
        assertEquals(dbEntry.getID(), entry.getID());
    }

    private void shouldBeEqual(TodoEntry first, TodoEntry second){
        assertEquals(first.getID(), second.getID());
        assertEquals(first.getText(), second.getText());
        assertEquals(first.getPosition(), second.getPosition());
        assertEquals(first.isDone(), second.isDone());
        assertEquals(first.isPrioritized(), second.isPrioritized());
    }

    public void testDelete() throws Exception {
        // Given
        TodoEntry entry = new TodoEntry(null, this.list, "Eins", false, false, 0);
        long id = this.adapter.insert(entry);
        assertTrue(this.adapter.byID(id) != null);
        // When
        boolean success = this.adapter.delete(entry);
        // Then
        assertTrue(success);
        assertTrue(this.adapter.byID(id) == null);
    }

    public void testDelete_nonexistingEntry(){
        // Given
        TodoEntry entry = new TodoEntry(null, this.list, "Eins", false, false, 0);
        // When
        boolean success = this.adapter.delete(entry);
        // Then
        assertFalse(success);
    }

    public void testDelete_nonexistingId(){
        // Given / When
        boolean success = this.adapter.delete(123l);
        // Then
        assertFalse(success);
    }

    public void testByID() throws Exception {
        // Given
        TodoEntry entry = new TodoEntry(null, this.list, "Eins", false, false, 0);
        // When
        long id = this.adapter.insert(entry);
        // Then
        TodoEntry dbEntry = this.adapter.byID(id);
        assertEquals(dbEntry.getID(), entry.getID());
        assertEquals(dbEntry.getPosition(), entry.getPosition());
        assertEquals(dbEntry.getText(), entry.getText());
    }
}