package de.fhw.things.todo.data.beans;

import android.support.annotation.Nullable;

import de.fhw.things.db.DBEntity;

/**
 * Ein Eintrag einer TodoListe.
 */
public class TodoEntry extends DBEntity {

    /**
     * Referenzierte TodoListe, null wenn noch nicht gespeichert
     */
    @Nullable
    private TodoList todoList;

    /** Beschreibungstext */
    private String text;

    /** Flag, ob die Aufgabe erledigt ist */
    private Boolean done;

    /** Flag, ob die Aufgabe priorisiert ist */
    private Boolean prioritized;

    /**
     * Position des Eintrags in der TodoListe, null wenn noch nicht gespeichert
     */
    @Nullable
    private Integer position;

    /**
     * Erstellt einen TodoEntry
     *
     * @param id          ID aus der Datenbank
     * @param todoList    Beinhaltende TodoListe
     * @param text        Text
     * @param done        Angabe ob erledigt
     * @param prioritized Angabe ob priorisiert
     * @param position    Position des Eintrags
     */
    public TodoEntry(@Nullable Long id, @Nullable TodoList todoList, String text, Boolean done, Boolean prioritized, @Nullable Integer position) {
        this.id = id;
        this.todoList = todoList;
        this.text = text;
        this.done = done;
        this.prioritized = prioritized;
        this.position = position;
    }

    /**
     * Erstellt einen TodoEintrag und initialisiert restliche Werte.
     *
     * @param text Text
     */
    public TodoEntry(String text) {
        this(null, null, text, false, false, null);
    }

    /**
     * Erstellt einen neuen TodoListen-Eintrag.
     *
     * @param todoList Beinhaltende TodoListe
     * @param text     Beschreibung
     * @param position Position des Eintrags
     */
    public TodoEntry(TodoList todoList, String text, Integer position) {
        this(null, todoList, text, false, false, position);
    }

    /**
     * @return eine Kopie des Eintrags.
     */
    public TodoEntry copy() {
        return new TodoEntry(this.id, this.todoList, this.text, this.done, this.prioritized, this.position
        );
    }

    @Override
    public String toString() {
        return "TodoEntry{" +
                "id=" + this.id +
                ", todoList=" + (this.todoList != null ? this.todoList.getTitle() : null) +
                ", text='" + this.text + '\'' +
                ", done=" + this.done +
                ", prioritized=" + this.prioritized +
                ", position=" + this.position +
                '}';
    }

    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TodoEntry todoEntry = (TodoEntry) o;

        if (this.todoList != null ? !this.todoList.equals(todoEntry.todoList) : todoEntry.todoList != null)
            return false;
        if (this.id != null ? !this.id.equals(todoEntry.id) : todoEntry.id != null) return false;
        if (this.text != null ? !this.text.equals(todoEntry.text) : todoEntry.text != null) return false;
        if (this.done != null ? !this.done.equals(todoEntry.done) : todoEntry.done != null) return false;
        return !(this.prioritized != null ? !this.prioritized.equals(todoEntry.prioritized) : todoEntry.prioritized != null) && !(this.position != null ? !this.position.equals(todoEntry.position) : todoEntry.position != null);

    }
    @Override
    public int hashCode() {
        int result = this.todoList != null ? this.todoList.hashCode() : 0;
        result = 31 * result + (this.id != null ? this.id.hashCode() : 0);
        result = 31 * result + (this.text != null ? this.text.hashCode() : 0);
        result = 31 * result + (this.done != null ? this.done.hashCode() : 0);
        result = 31 * result + (this.prioritized != null ? this.prioritized.hashCode() : 0);
        result = 31 * result + (this.position != null ? this.position.hashCode() : 0);
        return result;
    }

    /**
     * @return Beschreibung
     */
    public String getText() {
        return this.text;
    }

    /**
     * @param text zu setzender Text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return flag, ob der Eintrag erledigt ist
     */
    public Boolean isDone() {
        return this.done;
    }

    /**
     * @param done Angabe, ob der Eintrag erledigt ist
     */
    public void setDone(Boolean done) {
        this.done = done;
    }

    /**
     * @return Angabe, ob der Eintrag priorisiert ist
     */
    public Boolean isPrioritized() {
        return this.prioritized;
    }

    /**
     * @param prioritized Angabe, ob der Eintrag priorisiert ist
     */
    public void setPrioritized(Boolean prioritized) {
        this.prioritized = prioritized;
    }

    /**
     * @return Position des Eintrags
     */
    @Nullable
    public Integer getPosition() {
        return this.position;
    }

    /**
     * @param position Position des Eintrags
     */
    public void setPosition(@Nullable Integer position) {
        this.position = position;
    }

    /**
     * @return Zugehörige TodoListe
     */
    @Nullable
    public TodoList getTodoList() {
        return this.todoList;
    }

    /**
     * @param todoList Referenzierte TodoList
     */
    @SuppressWarnings("NullableProblems")
    public void setTodoList(TodoList todoList) {
        this.todoList = todoList;
    }
}
