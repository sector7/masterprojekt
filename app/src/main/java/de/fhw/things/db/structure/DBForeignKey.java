package de.fhw.things.db.structure;

/**
 * Repräsentation einer Spalte mit Fremdschlüssel.
 */
public class DBForeignKey extends DBColumn {

    /**
     * Konstruktor für neue Spalte mit Fremdschlüssel.
     *
     * @param referencedTable referenzierte Tabelle, deren Primary Key verwendet wird
     */
    public DBForeignKey(final DBTable referencedTable) {
        super(referencedTable.getName() + DBColumn.COLUMN_ID.getName(), INTEGER, new DBConstraint() {
            @Override
            public String getSQLiteDefinition() {
                return "REFERENCES " + referencedTable.getName();
            }
        });
    }
}
