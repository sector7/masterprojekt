package de.fhw.things.todo.view.singleList;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import de.fhw.things.R;
import de.fhw.things.todo.data.TodoListWrapper;
import de.fhw.things.todo.data.beans.TodoEntry;

import static de.fhw.things.logger.Log.TODO_LOGGER;

/**
 * Repräsentiert eine TodoListe. Für jede TodoListe wird eine Instanz eines
 * TodoListFragments erzeugt.
 */
public class TodoListFragment extends ListFragment implements View.OnClickListener {
    /** Adapter zur Anzeige der TodoListe in der ListActivity. */
    private TodoListViewAdapter adapter;

    /** Wrapper zur Arbeit auf einer TodoListe. */
    private TodoListWrapper todoListWrapper;

    /**
     * Erzeugt eine neue Instanz eines TodoListen-Fragmentes.
     *
     * @param ID ID der TodoListe
     */
    public static TodoListFragment newInstance(int ID) {
        TodoListFragment f = new TodoListFragment();

        // Supply ID input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", ID);
        f.setArguments(args);

        return f;
    }

    /**
     * Wird beim _erstmaligen_ Start der App ausgeführt. Wird nicht ausgeführt, wenn die App aufgeweckt wird.
     * Lädt die zuletzt betrachtete Liste und initialisiert den TodoListWrapper sowie
     * den TodoListAdapter.
     *
     * @see <a href="http://developer.android.com/reference/android/app/Fragment.html#onCreate%28android.os.Bundle%29">
     *     http://developer.android.com/reference/android/app/Fragment.html#onCreate%28android.os.Bundle%29</a>
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences p = getActivity().getPreferences(Context.MODE_PRIVATE);
        long lastTodoListID = p.getLong(getString(R.string.last_todolist_id), getResources().getInteger(R.integer
                .default_todolist_id));

        int mNum = getArguments() != null ? getArguments().getInt("num") : (int) lastTodoListID;
        this.todoListWrapper = new TodoListWrapper(getContext(), mNum);
        resetAdapter();
    }

    /**
     * Anwenden des Layouts einer (jeder) Todoliste auf die Listenansicht.
     * Wird direkt nach dem Erzeugen sowie nach jedem Aufwecken der App ausgeführt.
     *
     * @param inflater Wendet XML-Layout ein eine View an
     * @param container Aufrufende View des Fragmentes
     * @param savedInstanceState gecachte Daten der umliegenden TodoListen
     * @return View, auf die das Layout angewandt wurde
     *
     * @see <a href="http://developer.android.com/reference/android/app/Fragment.html#onCreateView%28android.view.LayoutInflater,%20android.view.ViewGroup,%20android.os.Bundle%29">
     *     http://developer.android.com/reference/android/app/Fragment.html#onCreateView%28android.view.LayoutInflater,%20android.view.ViewGroup,%20android.os.Bundle%29</a>
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_todolist, container, false);
        setListeners(v);
        return v;
    }

    /**
     * Initialisiert die Listener der Schaltflächen zum hinzufügen eines neuen TodoEintrags.
     * @param v View
     */
    private void setListeners(View v) {
        setAddNewTodoButtonListeners(v);
        setSoftKeyboardListeners(v);
    }

    /**
     * Setzt die Listener zur Behandlung von Tastatureingaben.
     * Ermöglicht das Speichern und Verlassen des Hinzufügen-EditTextes bei Betätigen der DONE-Tast auf der Tastatur.
     * @param v View
     */
    private void setSoftKeyboardListeners(View v) {
        final EditText editText = (EditText) v.findViewById(R.id.editTextNewTodo);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // Definieren, was bei Klick auf "Fertig" auf der Tastatur getan
                // werden soll
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (editText.getText().length() > 0) {
                        // Ebenfalls speichern, wenn etwas eingetragen ist
                        addEntry(editText);
                    }
                    // Fokus von EditText lösen und Tastatur verstecken
                    editText.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                }
                return false;
            }
        });
    }

    /**
     *  Listener auf den Button zum Hinzufügen eines Todos setzen
     * @param v View
     */
    private void setAddNewTodoButtonListeners(View v) {

        ImageButton b = (ImageButton) v.findViewById(R.id.buttonAddTodo);
        b.setOnClickListener(this);
    }

    /**
     * Lädt die Einträge der aktiven TodoListe.
     * Wird nach dem Aufbau des Layouts aufgerufen. Beim Wechsel zu vorher
     * geladenen Listen wird der gespeicherte Zustand mit übergeben.
     *
     * @param savedInstanceState Zuletzt verwendeter Zustand der aktuellen Liste
     * @see <a href="http://developer.android.com/reference/android/app/Fragment.html#onActivityCreated%28android.os.Bundle%29">
     *     http://developer.android.com/reference/android/app/Fragment.html#onActivityCreated%28android.os.Bundle%29</a>
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        resetAdapter();
    }

    /**
     * Behandelt alle Klick-Events auf beliebige Elemente innerhalb eines Fragments.
     * @param v View
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonAddTodo:
                addEntry(getView());
                break;
        }
    }

    /**
     * Fügt einen Eintrag in die Liste ein und aktualisiert anschließend die Datenbank.
     *
     * @param view View
     */
    private void addEntry(View view) {
        TextView input = (TextView) view.findViewById(R.id.editTextNewTodo);
        if ("".equals(input.getText().toString()))
            Toast.makeText(getContext(), R.string.hint_name_entry, Toast.LENGTH_SHORT)
                    .show();
        else {
            // Eintrag in die lokale Liste schreiben
            this.todoListWrapper.insertEntry(
                    new TodoEntry(input.getText().toString())
            );
            // Textfeld leeren
            input.setText("");
            // Aktualisierung der angezeigten Liste beim Zufügen eines Eintrages
            this.adapter.notifyDataSetChanged();

            TODO_LOGGER.v("Listeneintrag: " + input.getText().toString());
        }
    }

    /**
     * Lädt den ListViewAdapter neu und setzt ihn für diese ListView.
     */
    public void resetAdapter() {
        this.adapter = new TodoListViewAdapter(getActivity(), getView(), R.id.edit_text,
                this.todoListWrapper, this);
        setListAdapter(this.adapter);
    }
}
