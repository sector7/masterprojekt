package de.fhw.things.todo.data.beans;

import java.util.ArrayList;
import java.util.List;

import de.fhw.things.BuildConfig;
import de.fhw.things.db.DBEntity;

import static java.lang.Integer.signum;
import static java.lang.Math.abs;

/**
 * Objekt zur Repräsentation einer Todoliste.
 */
public class TodoList extends DBEntity {

    /** Einträge der Todoliste */
    private List<TodoEntry> entries;

    /** Titel der TodoListe. */
    private String title;

    /**
     * Konstruktorfunktion für eine neue Todoliste.
     *
     * @param title Titel der Todoliste
     */
    public TodoList(String title) {
        this(null, title, new ArrayList<TodoEntry>());
    }

    /**
     * Konstruktorfunktion für eine TodoListe aus der Datenbank.
     *
     * @param id      Id aus der Datenbank
     * @param title   Titel der TodoListe
     * @param entries Einträge der TodoListe
     */
    public TodoList(Long id, String title, ArrayList<TodoEntry> entries) {
        setID(id);
        setTitle(title);
        setEntries(entries);
    }

    /**
     * @return Titel der Todoliste
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * @param title Titel der Todoliste
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return Liste der Einträge
     */
    public List<TodoEntry> getEntries() {
        return this.entries;
    }

    /**
     * @param entries setzt alle Entries
     */
    public void setEntries(List<TodoEntry> entries) {
        this.entries = new ArrayList<>(entries);
        for (int i = 0; i < this.entries.size(); i++) {
            final TodoEntry entry = this.entries.get(i);
            entry.setTodoList(this);
            entry.setPosition(i);
        }
        assertPositions();
    }

    /**
     * Ersetzt den Eintrag an der gegebenen Position durch den übergebenen
     * @param pos   Position
     * @param entry neuer Eintrag
     * @return ersetzter Eintrag
     */
    public TodoEntry replaceEntry(int pos, TodoEntry entry) {
        TodoEntry toRemove = this.entries.get(pos);
        this.entries.remove(pos);
        toRemove.setPosition(null);
        toRemove.setTodoList(null);

        entry.setPosition(pos);
        entry.setTodoList(this);
        this.entries.add(pos, entry);
        assertPositions();
        return toRemove;
    }

    /**
     * @param entry zu löschender Eintrag
     */
    public void removeEntry(TodoEntry entry) {
        int pos = this.entries.indexOf(entry);
        entry.setPosition(null);
        entry.setTodoList(null);
        this.entries.remove(pos);

        updatePositions(pos, getSize(), -1);
        assertPositions();
    }


    /**
     * @return die Anzahl der Einträge in der Liste
     */
    public int getSize() {
        return this.entries.size();
    }

    /**
     * @param entry Fügt einen Eintrag hinzu
     */
    public void addEntry(TodoEntry entry) {
        entry.setPosition(this.entries.size());
        this.entries.add(entry);
        assertPositions();
    }

    /**
     * Aktualisiert die Positionsindizes der TodoListeneinträge, wenn ein Eintrag verschoben wird.
     * Alle Einträge zwischen alter und neuer Position rücken entsprechend nach
     *
     * @param oldPosition Indexposition des zu verschiebenden Eintrags
     * @param newPosition Neue Indexposition
     */
    public void moveEntryToPosition(int oldPosition, int newPosition) {
        int direction = Integer.compare(oldPosition, newPosition); // -1 für Verschiebung nach unten
        this.entries.add(newPosition, this.entries.remove(oldPosition)); // Tauschen der Reihenfolge
        this.entries.get(newPosition).setPosition(newPosition);

        // Alle Positionen der dazwischenliegenden Einträge aktualisieren
        updatePositions(oldPosition, newPosition, direction);
        assertPositions();
    }

    /**
     * Verschiebt eine Menge von Einträgen von Position to bis from.
     * @param to Start-Position
     * @param from End-Position
     * @param direction Richtung (nach oben: 1, nach unten: -1)
     */
    @SuppressWarnings("ConstantConditions")
    private void updatePositions(int to, int from, int direction) {
        if (BuildConfig.DEBUG) {
            if (abs(direction) != 1) throw new IllegalArgumentException("Richtung muss -1 oder 1 sein");
            if (signum(from - to) == direction) throw new IllegalArgumentException("Falsche Laufrichtung");
        }
        int i = from;
        while (i != to) {
            i += direction;
            TodoEntry entry = this.entries.get(i);
            entry.setPosition(entry.getPosition() + direction);
        }
    }

    /**
     * Prüft, ob die Positionen in der Datenbank valide sind.
     */
    @SuppressWarnings("ConstantConditions")
    private void assertPositions() {
        if (BuildConfig.DEBUG) {
            for (int i = 0; i < getEntries().size(); i++) {
                TodoEntry entry = getEntries().get(i);
                if (i != entry.getPosition()) {
                    throw new IllegalStateException("Entry an Listenposition " + i + " hat intern Position " + entry
                            .getPosition());
                }
            }
        }
    }
}
