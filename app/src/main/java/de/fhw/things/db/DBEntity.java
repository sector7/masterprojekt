package de.fhw.things.db;

import android.support.annotation.Nullable;

/**
 * Abstrakte Oberklasse für Objekte, die in der Datenbank gespeichert werden können.
 */
public abstract class DBEntity {
    /**
     * ID des Objektes in der zugehörigen Datenbank-Tabelle oder <tt>null</tt>,
     * falls das Objekt nicht in der Datenbank gespeichert ist.
     */
    @Nullable
    protected Long id = null;

    /**
     * @return ID des Objektes in der zugehörigen Datenbank-Tabelle oder <tt>null</tt>,
     * falls das Objekt nicht in der Datenbank gespeichert ist.
     */
    @Nullable
    public final Long getID() {
        return this.id;
    }

    /**
     * Legt die ID des Objektes z.B. nach dem Speichern in die Datenbank oder nach dem Löschen fest.
     *
     * @param newID Neue ID bzw. <tt>null</tt>, wenn das Objekt aus der Datenbank gelöscht wurde
     * @return Das Objekt mit der neuen ID
     */
    public final DBEntity setID(@Nullable Long newID) {
        if (this.id != null && newID != null) throw new IllegalStateException("Neuzuweisung der ID");
        this.id = newID;
        return this;
    }
}
