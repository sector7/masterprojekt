package de.fhw.things.logger;

/**
 * Klasse mit vorhandenen Loggern, die für Konsolenausgaben genutzt werden können.
 */
public class Log {

    /** Logger für Datenbankoperationen */
    public static final Logger DB_LOGGER = new Logger("DB");

    /** Logger für Tests */
    public static final Logger TEST_LOGGER = new Logger("Test");

    /** Logger für den TodoBereich */
    public static final Logger TODO_LOGGER = new Logger("ToDo");

}
