package de.fhw.things.db.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;

import de.fhw.things.db.structure.DB;
import de.fhw.things.db.structure.DBColumn;
import de.fhw.things.db.structure.DBConstraint;
import de.fhw.things.db.structure.DBForeignKey;
import de.fhw.things.db.structure.TableProperties;
import de.fhw.things.todo.data.beans.TodoEntry;
import de.fhw.things.todo.data.beans.TodoList;

import static de.fhw.things.db.adapter.TodoEntryDBAdapter.COLUMN.DONE;
import static de.fhw.things.db.adapter.TodoEntryDBAdapter.COLUMN.ID;
import static de.fhw.things.db.adapter.TodoEntryDBAdapter.COLUMN.POSITION;
import static de.fhw.things.db.adapter.TodoEntryDBAdapter.COLUMN.PRIORITIZED;
import static de.fhw.things.db.adapter.TodoEntryDBAdapter.COLUMN.TEXT;
import static de.fhw.things.db.adapter.TodoEntryDBAdapter.COLUMN.TODO_LIST;
import static de.fhw.things.db.structure.DB.getDB;

/**
 * Datenbankobjekt zur Arbeit auf einem einzelnen TodoListen-Eintrag
 */
public class TodoEntryDBAdapter extends TableAdapter<TodoEntry, TodoEntryDBAdapter.COLUMN> {
    /** Statisches Objekt zum Auslesen von Tabellen-Meta-Informationen */
    public static final TableProperties<COLUMN> P = new TableProperties<COLUMN>() {
        @Override
        protected Map<COLUMN, DBColumn> fillColumnMap() {
            EnumMap<COLUMN, DBColumn> columns = new EnumMap<>(COLUMN.class);
            columns.put(COLUMN.ID, DBColumn.COLUMN_ID);
            columns.put(TEXT, new DBColumn("text", DBColumn.TEXT, DBConstraint.NOT_NULL));
            columns.put(TODO_LIST, new DBForeignKey(TodoListDBAdapter.P.table));
            columns.put(DONE, new DBColumn("done", DBColumn.BOOLEAN));
            columns.put(PRIORITIZED, new DBColumn("prioritized", DBColumn.BOOLEAN));
            columns.put(POSITION, new DBColumn("position", DBColumn.INTEGER));
            return Collections.unmodifiableMap(columns);
        }

        @Override
        public String getTableName() {
            return "todoListEntry";
        }
    };

    /** statischer Adapter */
    private static TodoEntryDBAdapter TODOENTRY_DB_ADAPTER;

    /**
     * Konstruktor.
     *
     * @param db Datenbankanbindung
     */
    private TodoEntryDBAdapter(DB db) {
        super(db, P.table);
    }

    /**
     * @return DB Adapter
     */
    public static TodoEntryDBAdapter getTodoEntryDbAdapter(){
        if(TODOENTRY_DB_ADAPTER == null){
            TODOENTRY_DB_ADAPTER = new TodoEntryDBAdapter(getDB());
        }
        return TODOENTRY_DB_ADAPTER;
    }

    /**
     * @param context Context
     * @return DB Adapter
     */
    public static TodoEntryDBAdapter getTodoEntryDbAdapter(Context context){
        if(TODOENTRY_DB_ADAPTER == null){
            TODOENTRY_DB_ADAPTER = new TodoEntryDBAdapter(getDB(context));
        }
        return TODOENTRY_DB_ADAPTER;
    }

    /**
     * Statische Methode zum Einfügen eines TodoListen-Eintrags
     *
     * @param db    Datenbank, in der eingefügt wird
     * @param entry Einzufügender TodoListen-Eintrag
     * @return ID, unter der das Objekt gespeichert wurde oder -1 bei Fehler
     */
    public static long insert(SQLiteDatabase db, TodoEntry entry) {
        return db.insert(P.tableName, null, staticContentValues(entry));
    }

    /**
     * Bildet die Inhalte eines TodoListen-Eintrags auf eine Datenbank-kompatible Datenstruktur ab
     * @param entry Abzubildender TodoListen-Eintrag
     * @return Schlüssel-Wert Paare, die in die Datenbank geschrieben werden können
     */
    @SuppressWarnings("ConstantConditions")
    private static ContentValues staticContentValues(TodoEntry entry) {
        ContentValues values = new ContentValues(COLUMN.values().length-1);
        values.put(P.name(TODO_LIST), entry.getTodoList().getID());
        values.put(P.name(TEXT), entry.getText());
        values.put(P.name(DONE), entry.isDone());
        values.put(P.name(PRIORITIZED), entry.isPrioritized());
        values.put(P.name(POSITION), entry.getPosition());
        return values;
    }

    /**
     * Liefert die Liste der Einträge, die mit einer TodoListe verknüpft sind
     *
     * @param db       Datenbank für die Abfrage
     * @param todoList TodoListe, dessen Einträge abgefragt werden
     * @param child    Fremdreferenz auf ein bereits initialisiertes Kind
     * @return Einträge der TodoListe
     */
    @SuppressWarnings("ConstantConditions")
    static ArrayList<TodoEntry> byTodoList(SQLiteDatabase db, TodoList todoList, @Nullable TodoEntry child) {
        ArrayList<TodoEntry> entries = new ArrayList<>();
        Cursor c = db.query(P.tableName, P.table.getColumnNames(), P.name(TODO_LIST) + "=" + todoList.getID(),
                null, null, null, P.name(POSITION), null);
        while (c.moveToNext()) {
            if (child != null && c.getLong(c.getColumnIndex(P.name(ID))) == child.getID()) {
                entries.add(child);
            } else {
                entries.add(fromCursor(db, c, todoList));
            }
        }
        c.close();
        todoList.setEntries(entries);
        return entries;
    }

    /**
     * Erstellt einen TodoListen-Eintrag anhand einer Datenbank-Zeile
     *
     * @param db     Datenbankverbindung zur Auflösung etwaiger Fremdreferenzen
     * @param c      Cursor auf die auszulesende Datenbankzeile
     * @param parent schon initialisierte Fremdreferenz, oder <tt>null</tt>, falls diese instanziert werden muss
     * @return Der initialisierte TodoListen-Eintrag
     */
    private static TodoEntry fromCursor(SQLiteDatabase db, Cursor c, @Nullable TodoList parent) {
        return new TodoEntry(c.getLong(c.getColumnIndex(P.name(COLUMN.ID))),
                (parent != null) ? parent : TodoListDBAdapter.byID(db, c.getLong(c.getColumnIndex(P.name(TODO_LIST)))),
                c.getString(c.getColumnIndex(P.name(TEXT))),
                c.getInt(c.getColumnIndex(P.name(DONE))) == 1,
                c.getInt(c.getColumnIndex(P.name(PRIORITIZED))) == 1,
                c.getInt(c.getColumnIndex(P.name(POSITION))));
    }

    /**
     * Statische Aktualisierung eines Objekts, falls es schon in der Datenbank gespeichert war,
     * ansonsten wird es neu eingefügt.
     *
     * @param db    Datenbank
     * @param entry Neuer bzw. aktualisierter TodoListen-Eintrag
     */
    public static void upsert(SQLiteDatabase db, TodoEntry entry) {
        TableAdapter.upsert(db, P.tableName, entry, staticContentValues(entry));
    }

    /**
     * Statische Methode zum Löschen eines TodoList-Entries.
     *
     * @param db    Datenbank
     * @param entry Zu löschender Eintrag
     */
    public static void delete(SQLiteDatabase db, TodoEntry entry) {
        TableAdapter.delete(db, P.tableName, entry.getID());
    }

    @NonNull
    @Override
    ContentValues getContentValues(@NonNull TodoEntry entry) {
        return staticContentValues(entry);
    }

    @Override
    @Nullable
    public TodoEntry byID(long id) {
        final SQLiteDatabase readDB = this.db.getReadableDatabase();
        final Cursor c = readDB.query(P.tableName, P.table.getColumnNames(), "_id=" + id, null, null, null, null, null);
        final TodoEntry result = c.moveToFirst() ? fromCursor(readDB, c, null) : null;
        readDB.close();
        return result;
    }

    /** Aufzählung der Spalten */
    public enum COLUMN {
        ID, // obligatorische ID-Spalte
        TODO_LIST, // Fremdreferenz auf zugehörige TodoListe
        TEXT, // Inhalt des Eintrags
        DONE, // Eintrag abgehakt?
        PRIORITIZED, // Eintrag priorisiert?
        POSITION // Position des Eintrags auf der TodoListe
    }
}
