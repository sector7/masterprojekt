package de.fhw.things.db.structure;

/**
 * Repräsentation der Struktur einer Datenbankspalte.
 */
public class DBColumn implements DBObject {

    /** Konstante für ID-Spalte. */
    public static final DBColumn COLUMN_ID = new DBColumn("_id", Type.INTEGER, DBConstraint.ID);

    /** Datenbank-Typ für Ganzzahlen und boolesche Werte */
    public static final Type INTEGER = Type.INTEGER, BOOLEAN = INTEGER;

    /** Datenbank-Typ für Strings */
    public static final Type TEXT = Type.TEXT;

    /** Datenbank-Typ für Fließkommazahlen */
    public static final Type REAL = Type.REAL;
    /** Name der Spalte. */
    private final String name;
    /** SQLite Datentyp der Spalte. */
    private final Type type;
    /** Mögliche Restriktionen. */
    private final DBConstraint[] constraints;

    /**
     * Konstruktor für eine Datenbankspalte.
     *
     * @param name        Name der Spalte
     * @param type        SQLite-Datentyp der Spalte
     * @param constraints Mögliche Restriktionen
     */
    public DBColumn(final String name, final Type type, final DBConstraint... constraints) {
        this.name = name;
        this.type = type;
        this.constraints = constraints;

    }

    /**
     * Konstruktor für eine Datenbankspalte ohne Restriktionen.
     *
     * @param name Name der Spalte
     * @param type SQLite-Datentyp der Spalte
     */
    public DBColumn(final String name, final Type type) {
        this(name, type, new DBConstraint[]{});
    }

    @Override
    public final String getSQLiteDefinition() {
        final StringBuilder def = new StringBuilder(this.name + " " + this.type.toString());
        for (DBConstraint constraint : this.constraints) {
            def.append(' ').append(constraint.getSQLiteDefinition());
        }
        return def.toString();
    }

    /**
     * @return Identifizierender Name der Spalte
     */
    public final String getName() {
        return this.name;
    }

    /**
     * Aufzählung der Typen in SQLite.
     */
    private enum Type {
        INTEGER,
        TEXT,
        REAL
    }
}
