package de.fhw.things.todo.data;


import android.content.Context;

import java.util.List;

import de.fhw.things.todo.data.beans.TodoEntry;
import de.fhw.things.todo.data.beans.TodoList;

import static de.fhw.things.db.adapter.TodoEntryDBAdapter.getTodoEntryDbAdapter;
import static de.fhw.things.db.adapter.TodoListDBAdapter.getTodoListDBAdapter;

/**
 * Kapselt die Arbeit auf einem TodoList-Objekt und der Datenbank.
 */
public class TodoListWrapper {

    /** Aktuell angezeigte TodoListe */
    private TodoList todoList;

    /** Context */
    private final Context context;

    /**
     * Initialisiert die Datenbank-Adapter und lädt eine TodoListe.
     * @param todoListToLoad Die TodoListe, die zu Beginn gezeigt werden soll
     */
    public TodoListWrapper(Context context, long todoListToLoad) {
        this.context = context;
        loadTodoList(todoListToLoad);
    }

    /**
     * @param id Id der TodoListe, die geladen werden soll
     */
    private void loadTodoList(long id) {
        this.todoList = getTodoListDBAdapter(this.context).byID(id);
    }

    /**
     * @param entry Eintrag der der Todoliste und der DB hinzugefügt werden soll
     */
    public void insertEntry(TodoEntry entry) {
        entry.setTodoList(this.todoList);
        entry.setPosition(this.todoList.getSize());
        this.todoList.addEntry(entry);
        getTodoEntryDbAdapter().insert(entry);
    }

    /**
     * @param entry Eintrag, der gelöscht werden soll
     */
    public void removeEntry(TodoEntry entry) {
        this.todoList.removeEntry(entry);
        getTodoEntryDbAdapter().delete(entry);
    }

    /**
     * @param entry Eintrag, der aktualisiert werden soll.
     */
    public void updateEntry(TodoEntry entry) {
        for (int i = 0; i < this.todoList.getSize(); i++) {
            if ((entry.getID() != null)
                    && entry.getID().equals(this.todoList.getEntries().get(i).getID())) {
                this.todoList.replaceEntry(i, entry);
                break;
            }
        }
        getTodoEntryDbAdapter().update(entry);
    }

    /**
     * @return TodoEntries
     */
    public List<TodoEntry> getEntries() {
        return this.todoList.getEntries();
    }

    /**
     * @param position Position des gesuchten Eintrags
     * @return Eintrag an Position
     */
    public TodoEntry getEntry(int position){
        return this.todoList.getEntries().get(position);
    }

    /**
     * Update die Done-Eigenschaft des TodoEintrags.
     * @param entry Eintrag
     * @param checked Done oder nicht done, das ist hier die Frage
     */
    public void updateEntryDone(TodoEntry entry, boolean checked) {
        entry.setDone(checked);
        this.updateEntry(entry);
    }

    /**
     * Toggled die Priorität eines TodoEintrags.
     * Behandelt die Anzeige, triggert die Datenbankoperationen und aktualisiert die Ids
     * der mIdMap für den Array Adapter.
     * @param entry Eintrag
     */
    public void toggleEntryPriority(TodoEntry entry) {
        entry.setPrioritized(!entry.isPrioritized());
        this.updateEntry(entry);
    }

    /**
     * Aktualisiert den Text eines TodoEintrags, wenn sich dieser geändert hat.
     * Behandelt die Anzeige, triggert die Datenbankoperationen und aktualisiert die Ids
     * der mIdMap für den Array Adapter.
     * @param entry Eintrag
     * @param currentText akuell eingestellter Text
     */
    public void updateEntryText(TodoEntry entry, String currentText) {
        // Nur aktualisieren, wenn der Text geändert wurde und es sich um denselben Eintrag handelt
        if (!entry.getText().equals(currentText)) {
            entry.setText(currentText);
            this.updateEntry(entry);
        }
    }

    /**
     * Liefert einen TodoEintrag an gegebener Position.
     */
    public TodoEntry get(int position){
        return this.todoList.getEntries().get(position);
    }
}
