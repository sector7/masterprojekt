package de.fhw.things.db.structure;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.VisibleForTesting;

import com.google.common.collect.ImmutableMap;

import de.fhw.things.db.adapter.TodoEntryDBAdapter;
import de.fhw.things.db.adapter.TodoListDBAdapter;
import de.fhw.things.db.adapter.TrackingDBAdapter;

/**
 * Repräsentation der Datenbankstruktur
 */
public final class DB extends SQLiteOpenHelper implements DBObject {

    /** Dateiname der Datenbank */
    @VisibleForTesting
    public static final String DATABASE_NAME = "database.db";
    /** Sammlung der Tabellen, referenziert anhand ihres Tabellennamens */
    private static final ImmutableMap<String, DBTable> TABLE_MAP;
    /** Inkrementelle Version der Datenbank für etwaige Updates */
    private static final int DATABASE_VERSION = 2;
    /** DB Instanz zum Zugriff auf Datenbankobjekt */
    private static DB instance;

    static {
        final ImmutableMap.Builder<String, DBTable> builder = ImmutableMap.builder();
        builder.put(TodoListDBAdapter.P.tableName, TodoListDBAdapter.P.table);
        builder.put(TodoEntryDBAdapter.P.tableName, TodoEntryDBAdapter.P.table);
        builder.put(TrackingDBAdapter.P.tableName, TrackingDBAdapter.P.table);

        TABLE_MAP = builder.build();
    }

    /**
     * @see SQLiteOpenHelper#SQLiteOpenHelper(Context, String, SQLiteDatabase.CursorFactory, int)
     */
    private DB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Liefert oder instanziiert die DB.
     * @param context Kontext
     * @return DB Instanz
     */
    public static DB getDB(Context context) {
        if(instance == null){
            instance = new DB(context);
        }
        return instance;
    }

    /**
     * @return DB Instanz
     * @throws IllegalStateException wenn noch keine Instanz vorhanden
     */
    public static DB getDB(){
        if(instance == null){
            throw new IllegalStateException("DB wurde noch nicht instanziiert.");
        }
        return instance;
    }

    @Override
    public String getSQLiteDefinition() {
        return DATABASE_NAME;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (DBTable table : TABLE_MAP.values()) {
            table.create(db);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (String tableName : TABLE_MAP.keySet()) {
            db.execSQL("DROP TABLE IF EXISTS " + tableName);
        }
        onCreate(db);
    }
}
