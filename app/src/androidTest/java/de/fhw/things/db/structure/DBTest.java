package de.fhw.things.db.structure;

import android.database.Cursor;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import org.junit.After;
import org.junit.Before;

import de.fhw.things.logger.Log;

/**
 * Testes die grundlegenden DB-Funktionalität (Erstellen der Tabellen usw.)
 */
public class DBTest extends AndroidTestCase {

    /** Testee */
    private DB db;

    /** Fake Context */
    private RenamingDelegatingContext context;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.context = new RenamingDelegatingContext(getContext(), "test_");
        this.db = DB.getDB(this.context);
    }

    @After
    public void tearDown() throws Exception {
        this.context.getDatabasePath(DB.DATABASE_NAME).delete();
        this.db.close();
        super.tearDown();
    }

    public void testOnCreate() throws Exception {
        // see http://stackoverflow.com/questions/9647129/android-sqlite-show-tables
        Cursor c = this.db.getReadableDatabase().rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND " +
                "name !='android_metadata' AND name != 'sqlite_sequence' ORDER BY name", null);
        while (c.moveToNext())
            Log.TEST_LOGGER.d(c.getString(0));
        assertEquals(3, c.getCount());
        assertTrue(c.moveToFirst());
        assertEquals("todoList", c.getString(0));
        assertTrue(c.moveToNext());
        assertEquals("todoListEntry", c.getString(0));
        assertTrue(c.moveToNext());
        assertEquals("trackingEntry", c.getString(0));
        c.close();
    }
}