package de.fhw.things.trackinghistory;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.SortedSet;

import de.fhw.things.R;
import de.fhw.things.commons.UIFunctions;
import de.fhw.things.db.adapter.TrackingDBAdapter;
import de.fhw.things.todo.TodoActivity;
import de.fhw.things.tracking.TrackingActivity;
import de.fhw.things.tracking.data.TrackingEntry;
import de.fhw.things.trackinghistory.view.adapter.FilterSpinnerAdapter;
import de.fhw.things.trackinghistory.view.holder.TrackingEntryHolder;
import de.fhw.things.trackinghistory.view.holder.TrackingHistoryToolbarHolder;
import de.fhw.things.trackinghistory.view.listener.DatePickerListener;
import de.fhw.things.trackinghistory.view.listener.TimePickerListener;

import static de.fhw.things.commons.TimeFormatter.formatTime;
import static de.fhw.things.commons.UIFunctions.displayTimer;
import static de.fhw.things.commons.UIFunctions.hideSoftkeyboardAndRemoveFocus;
import static de.fhw.things.commons.UIFunctions.switchActivatedActivityButtons;
import static de.fhw.things.commons.UIFunctions.updateSuggestions;
import static de.fhw.things.db.adapter.TrackingDBAdapter.getTrackingDBAdapter;

/**
 * Acivity für die Anzeige der angelegten Zeiteinträge.
 */
public class TrackingHistoryActivity extends AppCompatActivity {

    /** Extrahiert alle Projektfelder aus einer Liste an Entry Holdern */
    private static final Function<TrackingEntryHolder, AutoCompleteTextView> EXTRACT_PROJECT = new Function<TrackingEntryHolder, AutoCompleteTextView>() {
        @Override
        public AutoCompleteTextView apply(@Nullable TrackingEntryHolder input) {
            return (input!=null) ? input.project : null;
        }
    };

    /** Extrahiert alle Projektbeschreibungen aus einer Liste an Entry Holdern */
    private static final Function<TrackingEntryHolder, AutoCompleteTextView> EXTRACT_DESCRIPTION = new Function<TrackingEntryHolder, AutoCompleteTextView>() {
        @Override
        public AutoCompleteTextView apply(@Nullable TrackingEntryHolder input) {
            return (input!=null) ? input.description : null;
        }
    };

    /** Maximal (nach) zu ladende Anzahl an Einträgen */
    private static final int SHOW_LIMIT = 10;

    /** Die ScrollView, in der die Liste der Tracking-Einträge enthalten ist */
    private ScrollView trackingHistoryScrollView;

    /** Die Liste der Tracking-Einträge */
    private LinearLayout trackingHistory;

    /** Die Liste der aller TrackingEntry-Holder */
    private List<TrackingEntryHolder> trackingEntryHolderList;

    /** Datenbankzugriff */
    private TrackingDBAdapter trackingEntryDBAdapter;

    /** Menge aller aktuell verwendeten Projektnamen */
    private SortedSet<String> projectNames;

    /** Menge aller aktuell verwendeten Projektbeschreibungen */
    private SortedSet<String> projectDescriptions;

    /** Button zum Nachladen von Elementen */
    private LinearLayout loadMoreButton;

    /** Holder für die Elemente der Toolbar */
    private TrackingHistoryToolbarHolder toolbarHolder;

    /** Löschmodus */
    private boolean selectionMode = false;
    /** Projekt nach dem aktuell gefiltert wird oder null */
    private String filteredProject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUI();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        initializeUI();
    }

    /**
     * Initialisiert das User Interface.
     */
    private void initializeUI() {
        setContentView(R.layout.activity_tracking_history);
        // Layout
        this.trackingEntryDBAdapter = getTrackingDBAdapter(getBaseContext());
        this.trackingHistoryScrollView = (ScrollView) this.findViewById(R.id.trackingHistoryScrollView);
        this.trackingHistory = (LinearLayout) this.findViewById(R.id.trackingHistory);
        this.loadMoreButton = (LinearLayout) findViewById(R.id.loadMorePanel);
        this.toolbarHolder = new TrackingHistoryToolbarHolder(this);

        // Daten laden
        initToolbar();
        loadSuggestionsFromDB();
        List<TrackingEntry> entries = this.trackingEntryDBAdapter.getAllByProject(null, SHOW_LIMIT, 0);
        populateTrackingHistory(entries);
        initToolbar();
        switchActivatedActivityButtons(this, this.getClass());
    }

    /**
     * Lädt weitere Einträge nach.
     * @param count Anzahl der nachzuladenden Einträge
     */
    private void loadMore(int count) {
        int currentlyShown = this.trackingEntryHolderList.size();
        this.trackingHistory.removeView(this.loadMoreButton);
        List<TrackingEntry> entries = this.trackingEntryDBAdapter.getAllByProject(
                filteredProject,
                count,
                currentlyShown
        );
        loadEntries(entries, currentlyShown);
        this.trackingHistory.invalidate();
    }

    /**
     * Lädt weitere Einträge nach.
     * @param view Gedrückte View
     */
    public void loadMore(View view) {
        loadMore(SHOW_LIMIT);
    }

    /**
     * Zeigt alle übergebenen Tracking-Einträge in der ScollView an.
     *
     * @param trackingEntries Tracking-Einträge
     */
    private void populateTrackingHistory(List<TrackingEntry> trackingEntries) {
        this.trackingEntryHolderList = new ArrayList<>();
        this.trackingHistory.removeAllViews();
        loadEntries(trackingEntries, this.trackingEntryHolderList.size());
        refreshSuggestions();
    }

    /**
     * Lädt die Vorschläge für den Projekt und Aufgabenbeschreibung aus der Datenbank und
     * cached sie.
     */
    private void loadSuggestionsFromDB() {
        this.projectNames = getTrackingDBAdapter().getAllProjectNames();
        this.projectDescriptions = getTrackingDBAdapter().getAllDescriptions();
    }

    /**
     * Verbindet den Cache der Autovervollständigung mit allen Einträge der Tracking-History.
     */
    private void refreshSuggestions(){
        updateSuggestions(this, this.projectNames, Lists.transform(this.trackingEntryHolderList, EXTRACT_PROJECT));
        updateSuggestions(this, this.projectDescriptions, Lists.transform(this.trackingEntryHolderList, EXTRACT_DESCRIPTION));
    }

    /**
     * Lädt bestimmte Einträge aus der übergebenen Liste und fügt sie an.
     * @param trackingEntries Liste
     * @param currentlyShown Anzahl der aktuell gezeigten Einträge
     */
    private void loadEntries(List<TrackingEntry> trackingEntries, int currentlyShown) {
        for(int i = currentlyShown; i < currentlyShown + trackingEntries.size(); i++) {
            TrackingEntry trackingEntry = trackingEntries.get(i - currentlyShown);
            TrackingEntryHolder newHolder = new TrackingEntryHolder(this, trackingEntry, i);

            // Listener auf Textfelder für Projektnamen und -beschreibung setzen
            addListenerToEditText(newHolder.project, true);
            addListenerToEditText(newHolder.description, false);

            // Listener zum Ein-/Ausblenden der Zeitstempel
            addListenerToDurationField(newHolder.duration);

            // Listener auf Datumsfelder setzen
            newHolder.beginDate.setOnClickListener(new DatePickerListener(this, trackingEntry, i, true));
            newHolder.endDate.setOnClickListener(new DatePickerListener(this, trackingEntry, i, false));

            // Listener auf Zeitfelder setzen
            newHolder.beginTime.setOnClickListener(new TimePickerListener(
                    this, trackingEntry, i, true, DateFormat.is24HourFormat(getApplicationContext())));
            newHolder.endTime.setOnClickListener(new TimePickerListener(
                    this, trackingEntry, i, false, DateFormat.is24HourFormat(getApplicationContext())));

            // Anzeige initialisieren
            toggleEntryCheckbox(this.selectionMode, newHolder);
            // Entry hinzufügen
            this.trackingEntryHolderList.add(newHolder);
            this.trackingHistory.addView(newHolder.entry);
        }
        // Load more Button ans Ende der Liste
        showLoadMoreIfMoreAvailable();
    }

    /**
     * Fügt einen 'mehr laden'-Button ans Ende der Tracking-History, falls noch mehr Einträge
     * in der Datenbank vorhanden sind, als aktuell angezeigt werden.
     */
    private void showLoadMoreIfMoreAvailable() {
        if(this.trackingEntryDBAdapter.getTotalEntryCount(this.filteredProject) > this.trackingEntryHolderList.size())
            this.trackingHistory.addView(this.loadMoreButton);
    }

    /**
     * Initialisiert die Toolbar.
     */
    private void initToolbar() {
        final TrackingEntry trackingEntry = this.trackingEntryDBAdapter.getRunningEntry();

        // Zur Zeit laufender Eintrag
        if(trackingEntry != null){
            this.toolbarHolder.time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new UIFunctions().switchActivity(TrackingHistoryActivity.this, this.getClass(), TrackingActivity.class);
                }
            });
            displayTimer(trackingEntry, this.toolbarHolder.time, getString(R.string.starttime));
        } else {
            this.toolbarHolder.time.setText(formatTime(this.trackingEntryDBAdapter.getTrackedTime(), true));
        }

        List<String> names = new ArrayList<>(this.trackingEntryDBAdapter.getAllProjectNames());
        names.add(0, getString(R.string.trackingHistory_allProjects));

        this.toolbarHolder.filterEntries.setAdapter(new FilterSpinnerAdapter(this, new ArrayList<>(names)));

        this.toolbarHolder.filterEntries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                hideSoftkeyboardAndRemoveFocus(getBaseContext(), TrackingHistoryActivity.this.trackingHistory);
                selectionMode(false);
                if (position == 0)
                    onNothingSelected(parent);
                else {
                    TrackingHistoryActivity.this.filteredProject = (String) parent.getItemAtPosition(position);
                    toolbarHolder.time.setText(
                            formatTime(TrackingHistoryActivity.this.trackingEntryDBAdapter.getTrackedTime(TrackingHistoryActivity.this.filteredProject), true));
                    populateTrackingHistory(TrackingHistoryActivity.this.trackingEntryDBAdapter.getAllByProject(TrackingHistoryActivity.this.filteredProject, SHOW_LIMIT, 0));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                hideSoftkeyboardAndRemoveFocus(getBaseContext(), TrackingHistoryActivity.this.trackingHistory);
                selectionMode(false);
                TrackingHistoryActivity.this.filteredProject = null;
                toolbarHolder.time.setText(formatTime(TrackingHistoryActivity.this.trackingEntryDBAdapter.getTrackedTime(), true));
                populateTrackingHistory(TrackingHistoryActivity.this.trackingEntryDBAdapter.getAllByProject(null, SHOW_LIMIT, 0));
            }
        });
    }
    /**
     * Setzt einen Listener auf ein Eingabefeld, sodass der Inhalt des Feldes (Projekt oder Beschreibung)
     * beim Verlassen des Feldes in die Datenbank geschrieben wird.
     * @param inputField Input Feld
     * @param isProject TRUE wenn Projektfeld der Auslöser, FALSE wenn Beschreibungsfeld der Auslöser
     */
    private void addListenerToEditText(EditText inputField, final boolean isProject) {

        inputField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                                @Override
                                                public void onFocusChange(View v, boolean hasFocus) {
                                                    TrackingEntryHolder trackingEntryHolder = TrackingHistoryActivity.this.trackingEntryHolderList.get((int) v.getTag());
                                                    if (!hasFocus) {
                                                        updateHolderAndDB(trackingEntryHolder, isProject);
                                                    }
                                                }
                                            }
        );

        inputField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    TrackingEntryHolder trackingEntryHolder = TrackingHistoryActivity.this.trackingEntryHolderList.get((int) v.getTag());
                    // Bei "Fertig" Fokus von EditText lösen und Tastatur verstecken
                    hideSoftkeyboardAndRemoveFocus(getBaseContext(), v);

                    updateHolderAndDB(trackingEntryHolder, isProject);
                }
                return false;
            }
        });
    }

    /**
     * Aktualisiert den Holder des bearbeiteten Eintrages und schreibt Änderungen in die Datenbank.
     * @param holder Hält das Tracking Entry bereit
     * @param isProject Unterscheidung zwischen Projekt- oder Aufgabenbeschreibung
     */
    private void updateHolderAndDB(TrackingEntryHolder holder, boolean isProject) {
        //Feld im Tracking-Eintrag aktualisieren
        if (isProject) {
            final String project = holder.project.getText().toString();
            holder.trackingEntry.setProject(project);
            // Vorschlagslisten aktualisieren
            this.projectNames.add(project);
            updateSuggestions(this, this.projectNames, Lists.transform(this.trackingEntryHolderList, EXTRACT_PROJECT));
        } else {
            final String description = holder.description.getText().toString();
            holder.trackingEntry.setDescription(holder.description.getText().toString());
            this.projectDescriptions.add(description);
            updateSuggestions(this, this.projectDescriptions, Lists.transform(this.trackingEntryHolderList, EXTRACT_DESCRIPTION));
        }

        // Neuen Tracking-Eintrag in die Datenbank schreiben
        updateTrackingEntryToDB(holder.trackingEntry);
    }

    /**
     * Setzt den Start- oder Endzeitpunkt eines Tracking-Eintrages auf den übergebenen Wert. Wenn
     * der Startzeitpunkt über das Ende hinaus geschoben wird, wird das Ende entsprechend der
     * vorigen Dauer ebenfalls verschoben. Analog, falls der neue Endzeitpunkt vor den aktuellen
     * Startzeitpunkt geschoben wird.
     * @param newDate Neuer Zeitpunkt
     * @param tag Nummer des Tracking-Eintrages
     * @param isBegin Handelt es sich um einen Startzeitpunkt?
     */
    public void setDate(Calendar newDate, int tag, boolean isBegin) {
        TrackingEntryHolder currentTrackingEntryHolder = this.trackingEntryHolderList.get(tag);
        TrackingEntry currentTrackingEntry = currentTrackingEntryHolder.trackingEntry;

        // Ändern des Startzeitpunktes
        if (isBegin) {
            if(newDate.after(currentTrackingEntry.getEnd())) {
                // Der User hat seinen neuen Startzeitpunkt hinter aktuelles Ende geschoben
                long currentDelta = currentTrackingEntry.getDuration();
                // Endzeitpunkt auf (NeuerStart + altesDelta) setzen
                Calendar end = currentTrackingEntry.getEnd();
                if(end == null) {
                    end = Calendar.getInstance();
                    currentTrackingEntry.setEnd(end);
                }
                end.setTimeInMillis(newDate.getTimeInMillis() + currentDelta);
            }

            currentTrackingEntry.setBegin(newDate);

        } else { // Ändern des Endzeitpunktes
            if(newDate.before(currentTrackingEntry.getBegin())) {
            // Der User hat seinen neuen Endzeitpunkt vor den aktuellen Start geschoben
                long currentDelta = currentTrackingEntry.getDuration();
                // Startzeitpunkt auf (NeuesEnde - altesDelta) setzen
                currentTrackingEntry.getBegin().setTimeInMillis(newDate.getTimeInMillis() - currentDelta);
            }

            currentTrackingEntry.setEnd(newDate);
        }

        currentTrackingEntryHolder.setTimestampsToViews();
        updateTrackingEntryToDB(currentTrackingEntry);
        toolbarHolder.time.setText(
                formatTime(TrackingHistoryActivity.this.trackingEntryDBAdapter.getTrackedTime(TrackingHistoryActivity.this.filteredProject), true));
    }




    /**
     * Fügt einen onClick-Listener zur Anzeige der Aufgabendauer hinzu.
     * Ein Tap auf die Aufgabendauer blendet den Start- und Endzeitpunkt ein oder aus.
     * @param duration Dauer
     */
    private void addListenerToDurationField(TextView duration) {
        duration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleTrackingEntryTimestamps(v);
                hideSoftkeyboardAndRemoveFocus(getBaseContext(), TrackingHistoryActivity.this.trackingHistoryScrollView);
            }
        });
    }

    /**
     * Blendet die Zeitstempel unter einem Tracking-Eintrag ein oder aus.
     * @param entryView View des Tracking-Eintrages.
     */
    private void toggleTrackingEntryTimestamps(View entryView) {
        LinearLayout interval = TrackingHistoryActivity.this.trackingEntryHolderList.get((int) entryView.getTag()).interval;
        ViewGroup.LayoutParams intervalParams = interval.getLayoutParams();

        if (intervalParams.height
                == ViewGroup.LayoutParams.WRAP_CONTENT)
            showTrackingEntryTimestamps(entryView, false);
        else
            showTrackingEntryTimestamps(entryView, true);
    }

    /**
     * Setzt die Sichtbarkeit der Zeitstempel unter einem Tracking-Eintrag.
     * @param entryView View des Tracking-Eintrages
     * @param show True zum Einblenden der Zeitstempel
     */
    private void showTrackingEntryTimestamps(View entryView, boolean show) {
        LinearLayout interval = TrackingHistoryActivity.this.trackingEntryHolderList.get((int) entryView.getTag()).interval;
        ViewGroup.LayoutParams intervalParams = interval.getLayoutParams();
        // Wenn sichtbar
        if(!show) {
            intervalParams.height = 0;
            interval.setLayoutParams(intervalParams);
        } else {
            intervalParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            interval.setLayoutParams(intervalParams);
        }
        TrackingHistoryActivity.this.trackingHistory.invalidate();
    }

    /**
     * Wechselt den globalen Status zum Anzeigen der Selektionsboxen
     * @param view View, die gedrückt wurde
     */
    public void toggleSelectionMode(View view) {
        hideSoftkeyboardAndRemoveFocus(getBaseContext(), this.trackingHistoryScrollView);

        this.selectionMode = !this.selectionMode;
        selectionMode(this.selectionMode);
    }

    /**
     * Löscht alle markierten Tracking-Einträge aus der Datenbank und entfernt sie aus der
     * View.
     * @param view View
     */
    public void deleteSelectedEntries(View view) {
        hideSoftkeyboardAndRemoveFocus(getBaseContext(), this.trackingHistoryScrollView);
        List<TrackingEntryHolder> toBeDeleted = new ArrayList<>();

        for(TrackingEntryHolder holder : this.trackingEntryHolderList)
            if(holder.deleteMark.isChecked()) {
                this.trackingEntryDBAdapter.delete(holder.trackingEntry);
                this.trackingHistory.removeView(holder.entry);
                // Zu löschende Einträge des Holders merken
                toBeDeleted.add(holder);
            }
        // Holder aufräumen
        this.trackingEntryHolderList.removeAll(toBeDeleted);

        // Tags aktualisieren, falls mittige Einträge der Liste gelöscht wurden
        for(int i = 0; i < this.trackingEntryHolderList.size(); i++)
            this.trackingEntryHolderList.get(i).setTags(i);

        // Einträge nachladen, wenn nun zu wenige angezeigt werden
        int currentlyShown = this.trackingEntryHolderList.size();
        if(currentlyShown < SHOW_LIMIT)
            loadMore(SHOW_LIMIT - currentlyShown);

        cancelSelection(view);
        toolbarHolder.time.setText(
                formatTime(TrackingHistoryActivity.this.trackingEntryDBAdapter.getTrackedTime(TrackingHistoryActivity.this.filteredProject), true));
    }

    /**
     * Beendet den Selektierungsmodus der Tracking-Einträge.
     * @param view View, die gedrückt wurde
     */
    public void cancelSelection(View view) {
        this.selectionMode = false;
        selectionMode(false);
    }

    /**
     * Blendet die Selektionsboxen ein oder aus.
     * @param show Sichtbarkeit
     */
    private void selectionMode(boolean show) {
        this.toolbarHolder.selectionModeButton.setVisibility(show ? View.GONE : View.VISIBLE);
        this.toolbarHolder.cancelSelectionButton.setVisibility(show ? View.VISIBLE : View.GONE);
        this.toolbarHolder.deleteButton.setVisibility(show ? View.VISIBLE : View.INVISIBLE);

        for (TrackingEntryHolder holder : this.trackingEntryHolderList) {
            toggleEntryCheckbox(show, holder);
            // Kreuze ausblenden, wenn selection mode geschlossen wird
            if(!show)
                holder.deleteMark.setChecked(show);
        }

        TrackingHistoryActivity.this.trackingHistory.invalidate();
    }

    /**
     * Switcht zwischen der Anzeige von Checkbox oder Playbutton.
     * @param showCheckbox Angabe, ob die checkbox eingeblendet werden soll
     * @param holder EntryHolder
     */
    private void toggleEntryCheckbox(boolean showCheckbox, TrackingEntryHolder holder) {
        holder.restartTracking.setVisibility(showCheckbox ? View.GONE : View.VISIBLE);
        holder.deleteMark.setVisibility(showCheckbox ? View.VISIBLE : View.GONE);
    }

    /**
     * Fügt einen leeren Tracking-Eintrag ohne Dauer in die Datenbank ein. Lädt die Liste der
     * Tracking-Einträge anschließend neu.
     */
    public void addEmptyTrackingEntry(View view) {
        hideSoftkeyboardAndRemoveFocus(getBaseContext(), TrackingHistoryActivity.this.trackingHistory);
        updateTrackingEntryToDB(new TrackingEntry(Calendar.getInstance(), Calendar.getInstance()));
        List<TrackingEntry> entries = this.trackingEntryDBAdapter.getAll();
        populateTrackingHistory(entries);
        TrackingHistoryActivity.this.trackingHistoryScrollView.invalidate();

        // Zeitstempel des gerade hinzugefügten Eintrages einblenden
        showTrackingEntryTimestamps(this.trackingEntryHolderList.get(0).entry, true);

        // Cursor ins Projektfeld setzen und Tastatur einblenden
        this.trackingEntryHolderList.get(0).project.requestFocus();
        InputMethodManager keyboard = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.showSoftInput(this.trackingEntryHolderList.get(0).project, 0);
    }

    /**
     * Startet das Tracking
     * @param view gedrückter Button
     */
    public void restartTracking(View view){
        view.setActivated(true);
        Intent intent = new Intent(this, TrackingActivity.class);
        TrackingEntryHolder holder = this.trackingEntryHolderList.get((int) view.getTag());
        intent.putExtra("project", holder.project.getText().toString());
        intent.putExtra("description", holder.description.getText().toString());
        new UIFunctions().switchActivity(this, TrackingActivity.class, intent);
    }

    /**
     * Aktualisiert einen Tracking-Eintrag in der Datenbank. Der Eintrag wird erstellt,
     * wenn er noch nicht vorhanden ist.
     * @param trackingEntry Zeiteintrag
     */
    private void updateTrackingEntryToDB(TrackingEntry trackingEntry) {
        this.trackingEntryDBAdapter.upsert(trackingEntry);
    }

    /**
     * Startet die Tracking-Activity.
     * @param view Button
     */
    public void startTrackingActivity (View view) {
        new UIFunctions().switchActivity(this, TrackingHistoryActivity.class, TrackingActivity.class);
    }

    /**
     * Startet die TodoAcitvity.
     * @param view Button
     */
    public void startTodoActiviy(View view) {
        new UIFunctions().switchActivity(this, TrackingHistoryActivity.class, TodoActivity.class);
    }

    /**
     * Startet die TrackingHistory-Activity
     * @param view Button
     */
    public void startTrackingHistoryActivity(View view) {
        // Hier passiert nichts
    }

}
