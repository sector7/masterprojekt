package de.fhw.things.trackinghistory.view.listener;

import android.app.TimePickerDialog;
import android.view.View;
import android.widget.TimePicker;

import java.util.Calendar;

import de.fhw.things.tracking.data.TrackingEntry;
import de.fhw.things.trackinghistory.TrackingHistoryActivity;

/**
 * Listener zur Anzeige des Dialogs zu Angabe einer Uhrzeit.
 */
public class TimePickerListener implements View.OnClickListener, TimePickerDialog.OnTimeSetListener {
    /** Zugehörige Activity */
    private final TrackingHistoryActivity parent;
    /** zugehöriger Tracking-Eintrag */
    private final TrackingEntry entry;
    /** Frontend-Tag des Eintrags */
    private final int tag;
    /** Flag, ob Anfangs-Zeitpunkt (sonst Endzeitpunkt) */
    private final boolean isBegin;
    /** Flag zur Angabe der Formatierung (24er oder 12er Format) */
    private final boolean is24Hour;

    /**
     * Erzeugt einen Listener auf einem Textfeld, das eine Uhrzeit repräsentiert.
     * @param parent aufrufende Activity
     * @param entry Zeiteintrag
     * @param tag Frontend-Id des Eintrags
     * @param isBegin Angabe, ob der Eintrag der Anfangszeitpunkt ist
     * @param is24Hour 24-Stunden-Format des Benutzers
     */
    public TimePickerListener(TrackingHistoryActivity parent, TrackingEntry entry, int tag, boolean isBegin, boolean is24Hour) {
        this.parent = parent;
        this.entry = entry;
        this.tag = tag;
        this.isBegin = isBegin;
        this.is24Hour = is24Hour;
    }

    @Override
    public void onTimeSet(TimePicker view, int hour, int minute) {
        Calendar current;
        if(isBegin)
            current = this.entry.getBegin();
        else
            current = this.entry.getEnd();

        // Nur etwas tun, wenn das Datum sich verändert hat
        if((current != null) && (current.get(Calendar.HOUR_OF_DAY) != hour
                || current.get(Calendar.MINUTE) != minute)) {

            Calendar newDate = Calendar.getInstance();
            newDate.setTimeInMillis(current.getTimeInMillis());

            newDate.set(Calendar.HOUR_OF_DAY, hour);
            newDate.set(Calendar.MINUTE, minute);

            parent.setDate(newDate, this.tag, this.isBegin);
        }
    }

    @Override
    public void onClick(View v) {
        Calendar current;
        if(isBegin)
            current = this.entry.getBegin();
        else
            current = this.entry.getEnd();

        if(current != null) {
            TimePickerDialog dialog = new TimePickerDialog(
                    v.getContext(),
                    this,
                    current.get(Calendar.HOUR_OF_DAY),
                    current.get(Calendar.MINUTE),
                    this.is24Hour);
            dialog.show();
        }
    }

}