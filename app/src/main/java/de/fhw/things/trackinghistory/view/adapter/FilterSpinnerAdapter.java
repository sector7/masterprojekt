package de.fhw.things.trackinghistory.view.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.List;

import de.fhw.things.R;

/**
 * Ermöglicht verschiedene Formatierungen im Dropdown und der Anzeige des Projektfilters.
 */
public class FilterSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {
    /** Einträge des Dropdowns */
    private List<String> values;
    /** Kontext */
    private Context context;

    /**
     * Erzeugt den Adapter und setzt Werte und Kontext.
     */
    public FilterSpinnerAdapter(Context context, List<String> values) {
        this.values = values;
        this.context = context;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = (TextView) View.inflate(context, R.layout.layout_trackinghistory_filter, null);
        textView.setText(values.get(position));
        return textView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView textView = (TextView) View.inflate(context, R.layout.layout_trackinghistory_filter_dropdown, null);
        textView.setText(values.get(position));
        return textView;
    }
}