package de.fhw.things.db.structure;

import java.util.Map;

/**
 * Abstrakte Klasse für den einheitlichen Zugriff auf statische Tabellen-Meta-Informationen
 * @param <E> Aufzählungsdatentyp der Tabellenspalten
 */
public abstract class TableProperties<E extends Enum> {
    /** Name der referenzierten Tabelle */
    public final String tableName;
    /** Abbildung der Aufzählung auf die konkreten Tabellenspalten */
    public final Map<E, DBColumn> columns;
    /** Tabellendefinition */
    public final DBTable<E> table;

    /**
     * Initialisiert die finalen Felder der Klasse mithilfe der Methoden der implementierenden Klassen.
     */
    public TableProperties() {
        this.tableName = getTableName();
        this.columns = fillColumnMap();
        this.table = initTable();
    }

    /**
     * @return Referenz auf die Tabellenstruktur. Im einfachen Fall wird eine Standard-Tabelle angelegt, kann aber von
     * beerbenden Klassen angepasst werden.
     */
    protected DBTable<E> initTable() {
        return new DBTable<>(this);
    }

    /**
     * Liefert den Namen einer Tabellenspalte.
     *
     * @param column Spalte, deren Name erfragt werden soll
     * @return Name der Tabellenspalte in der Datenbank
     */
    public final String name(E column) {
        return this.columns.get(column).getName();
    }

    /**
     * Erstellt die Abbildung der Spalten
     * @return vollständige Abbildung der Aufzählung der Spalten auf die konkreten Spalten
     */
    protected abstract Map<E, DBColumn> fillColumnMap();

    /**
     * @return Name der Tabelle in der Datenbank
     */
    public abstract String getTableName();
}
