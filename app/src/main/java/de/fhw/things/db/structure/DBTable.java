package de.fhw.things.db.structure;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import static de.fhw.things.logger.Log.DB_LOGGER;

/**
 * Klasse zur Verwaltung einer Datenbanktabelle.
 */
public class DBTable<E extends Enum> implements DBObject {

    /** Eigenschaften der Tabelle */
    private final TableProperties<E> properties;

    /**
     * Konstruktor für eine Datenbanktabelle.
     *
     * @param properties Tabelleneigenschaften
     */
    public DBTable(TableProperties<E> properties) {
        this.properties = properties;
    }

    /**
     * Erstellt die Tabelle in der Datenbank.
     * @param db Zum Schreiben geöffnete Datenbankverbindung
     */
    public void create(SQLiteDatabase db) {
        DB_LOGGER.v("Erstelle Datenbanktabelle " + this.properties.tableName + ".");
        db.execSQL("CREATE TABLE " + getSQLiteDefinition());
    }

    /**
     * @return Tabellenname
     */
    public final String getName() {
        return this.properties.tableName;
    }

    /**
     * @return Spalten in dieser Tabelle
     */
    public final Map<E, DBColumn> getColumns() {
        return this.properties.columns;
    }

    @Override
    public final String getSQLiteDefinition() {
        final StringBuilder sql = new StringBuilder(getName()).append(" (");
        for (DBColumn column : this.properties.columns.values()) {
            sql.append(column.getSQLiteDefinition()).append(", ");
        }
        return sql.replace(sql.length() - 2, sql.length(), ")").toString();
    }

    @Override
    public final String toString() {
        return getSQLiteDefinition();
    }

    /**
     * Liefert die Namen der Spalten als Array
     *
     * @return Array mit den Spaltennamen
     */
    public String[] getColumnNames() {
        final String[] columnNames = new String[this.properties.columns.size()];
        int i = 0;
        for (DBColumn col : this.properties.columns.values()) {
            columnNames[i++] = col.getName();
        }
        return columnNames;
    }

}
