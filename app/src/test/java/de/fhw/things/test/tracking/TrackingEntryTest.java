package de.fhw.things.test.tracking;

import junit.framework.TestCase;

import java.util.Calendar;

import de.fhw.things.tracking.data.TrackingEntry;

/**
 * Test rund um die Zeit-Einträge.
 */
public class TrackingEntryTest extends TestCase {

    private Calendar c1, c2;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        this.c1 = Calendar.getInstance();
        this.c2 = Calendar.getInstance();
        Calendar c3 = Calendar.getInstance();
        Calendar c4 = Calendar.getInstance();
        this.c2.add(Calendar.HOUR_OF_DAY, 2);
        this.c2.add(Calendar.MINUTE, 30);
        c3.add(Calendar.DAY_OF_MONTH, 1);
        c3.add(Calendar.HOUR, 10);
        c4.add(Calendar.DAY_OF_MONTH, 3);
        c4.add(Calendar.HOUR, 11);
    }


    public void testConstructorDefault() throws Exception {
        TrackingEntry entry = new TrackingEntry();
        assertNotNull(entry.getBegin());
        assertTrue(entry.isRunning());
        assertFalse(Calendar.getInstance().before(entry.getBegin()));
    }

    public void testSetEnd() throws Exception {
        TrackingEntry entry = new TrackingEntry(this.c1, this.c2);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.HOUR, -5);
        try {
            entry.setEnd(c);
            fail();
        } catch (IllegalArgumentException e) {
            // pass
        }
    }

    public void testConstructorCalendarCalendar() throws Exception {
        TrackingEntry entry = new TrackingEntry(this.c1, this.c2);
        assertNotNull(entry.getBegin());
        assertNotNull(entry.getEnd());
        assertFalse(entry.isRunning());

        try {
            new TrackingEntry(this.c2, this.c1);
            fail();
        } catch (IllegalArgumentException e) {
            // pass
        }

        try {
            new TrackingEntry(null, this.c1);
            fail();
        } catch (NullPointerException e) {
            // pass
        }
    }

    public void testSetBegin() throws Exception {
        TrackingEntry entry = new TrackingEntry(this.c1, this.c2);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.HOUR, 5);
        try {
            entry.setBegin(c);
            fail();
        } catch (IllegalArgumentException e) {
            // pass
        }
    }

    public void testStopEntry() throws Exception {
        TrackingEntry entry = new TrackingEntry();
        assertTrue(entry.isRunning());
        assertFalse(entry.getBegin() == null);
        // When
        entry.stopTracking();
        // Then
        assertFalse(entry.isRunning());
        assertFalse(entry.getEnd() == null);
    }

    public void testGetDuration() throws Exception {
        assertEquals((2 * 60 + 30) * 60 * 1000, new TrackingEntry(this.c1, this.c2).getDuration());
        assertEquals(0, new TrackingEntry().getDuration());
    }

    public void testIsRunning() throws Exception {
        assertTrue(new TrackingEntry().isRunning());
        assertFalse(new TrackingEntry(this.c1, this.c2).isRunning());
    }
}