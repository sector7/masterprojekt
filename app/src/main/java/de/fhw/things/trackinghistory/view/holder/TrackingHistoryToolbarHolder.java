package de.fhw.things.trackinghistory.view.holder;

import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import de.fhw.things.R;

/**
 * Holder zum Zugriff auf einen laufenden Tracking-Eintrag.
 */
public class TrackingHistoryToolbarHolder {

    /** Gesamte Komponente */
    public final LinearLayout layout;
    /** Textfeld zur Anzeige der Zeit */
    public final TextView time;
    /** Suchfeld zur Filterung nach Projekten */
    public final Spinner filterEntries;
    /** Button zum einblenden der Löschen-Checkboxen bzw. zum Ausführen des Löschens */
    public final ImageButton deleteButton;
    /** Button zum Aktivieren des Selektionsmodus. */
    public final ImageButton selectionModeButton;
    /** Button zum hinzufügen eines neuen Eintrags */
    public final ImageButton addNewTrackingEntryButton;
    /** Button zum Abbruch des Löschvorgangs */
    public final ImageButton cancelSelectionButton;

    /**
     * Erstellt das Layout für einen laufenden Tracking Eintrag, setzt benötigte Callbacks.
     */
    public TrackingHistoryToolbarHolder(AppCompatActivity parent){
        this.layout = (LinearLayout) parent.findViewById(R.id.trackingHistory_toolbar);
        this.time = (TextView) parent.findViewById(R.id.trackingHistory_time);
        this.selectionModeButton = (ImageButton) parent.findViewById(R.id.trackingHistory_buttonSelectEntries);
        this.filterEntries = (Spinner) parent.findViewById(R.id.trackingHistory_filterEntries);
        this.deleteButton = (ImageButton) parent.findViewById(R.id.trackingHistory_buttonDelete);
        this.addNewTrackingEntryButton = (ImageButton) parent.findViewById(R.id.trackingHistory_buttonAddEntry);
        this.cancelSelectionButton = (ImageButton) parent.findViewById(R.id.trackingHistory_buttonCancelSelection);
    }

}
