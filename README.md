# Android App Things #

## Projektübersicht ##

Im Rahmen des Masterprojekts im Studiengang Informatik soll eine Android-App zur Produktivitätssteigerung implementiert werden. Diese soll Funktionen zur Unterstützung des selbstorganisa-torischen Arbeitens, wie Aufgabenplanung und Zeiteinteilung, bereitstellen.

## Funktionen ##
Die Android App stellt drei verschiedene Funktionensbereiche bereit:

### Todos ###
Zur Organisation offener Aufgaben kann eine einfache Liste zum schnellen Anlegen und Abhaken erstellt werden.

### Tracking ###
Das Tracking dient der Erfassung der Zeit, die für einzelne Aufgaben und Projekte aufgewendet wurde.

### Auswertung ###
Die Auswertung bietet eine Übersicht, wie viel Zeit für welche Teilbereiche aufgewendet wurde.